
  // edit staff dropzone
   Dropzone.autoDiscover = false;
    $( document ).ready(function() { 
      var photo_upload2 = new Dropzone("#edit_dropzone", {
          url: "AdminStaffController/saveImage",
          maxFilesize: 50,
          maxFiles: 50,
          method: "post",
          acceptedFiles: ".jpg,.jpeg,.png",
          paramName: "userfile",
          dictInvalidFileType: "Type file ini tidak dizinkan",
          addRemoveLinks: true,
          init: function () {
              thisDropzone = this;
              this.on("success", function (file, json) {
                  var obj = json;
                  $('.previews2').
                          append(
                              "<input type='hidden' name='image2[]' value='" + obj + "'>\n\
                             <input type='hidden' name='width2[]' value='" + file.width + "'>\n\
                             <input type='hidden' name='height2[]' value='" + file.height + "'>"
                                  );

              });
          } 
      });
    });


    // delete staff image
     $(document).on('click', '.btn_delete_staff_image', function(){         
         var id=$(this).data("staff_image_id");
          if(confirm("Are you sure you want to delete this?"))  
          {  
            $.ajax({  
             url:"admin/delete-staff-image",  
             method:"post",  
             data:{staff_image_id:id},
             dataType:"text",  
             success:function(data){  
                      //$("#"+id).fadeOut(); 
					  location.reload();
                    }  
            });  
          }  
        }); 


    // edit form validation
     $(document).ready(function(){      
      $("#profile").validate({
        rules: {    
          staff_name: {
            required: true
          }, 
                
        }
    });
  })
   $('#profile').on('submit', function(){
     $("#profile").valid();
   });