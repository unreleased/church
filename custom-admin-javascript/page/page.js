 $(document).ready(function () {
            CKEDITOR.replace('page_content');
    });


 //edit page
   $('.edit_page').on('click', function(){
          var page_key   = $(this).closest('tr').attr('id');
         $.ajax({
              url:'admin/render_edit_page',
              method:'POST',
              data:{page_key:page_key},
              success:function(data){
                 var result = JSON.parse(data);
                 $('#edit_page_div').html(result.edit_page_div);
              }
          })
      });

// add form validation
  $(document).ready(function(){      
      $("#addPage").validate();
  })
   $('#addPage').on('submit', function(){
     $("#addPage").valid();
   });


     // instant view image when select image for upload
   function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#viewFeatureImage')
                        .attr('src', e.target.result)
                        .width(200)
                        
                };

                reader.readAsDataURL(input.files[0]);
            }
        }


   // dataTable    
  document.addEventListener('DOMContentLoaded', function () { 
    var table;
      table = $('#blogTable').DataTable({
          "ordering": false

      });
  });





 // delete Blog
$(document).on('click', '.btn_delete_page', function(){         
         var id=$(this).data("delete_page");
         alert(id);
         var x=$(this); 
          if(confirm("Are you sure you want to delete this?"))  
          {  
            $.ajax({  
             url:"admin/delete-page",  
             method:"post",  
             data:{page_key:id},
             dataType:"text",  
             success:function(data){
                       x.closest('tr').fadeOut(); 
                    }  
            });  
          }  
        }); 




