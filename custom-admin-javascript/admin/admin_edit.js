// edit form validation
  $(document).ready(function(){      
      $("#editAdmin").validate({
        rules: {    
          username: {
            required: true
          }, 
          email: {
            required: true,
            email: true,
          },
                 
        }
    });
  })
   $('#editAdmin').on('submit', function(){
     $("#editAdmin").valid();
   });



   // image upload script by dropzone
     Dropzone.autoDiscover = false;
          $( document ).ready(function() { 
            var photo_upload2 = new Dropzone("#edit_dropzone", {
                url: "AdminController/saveImage",
                maxFilesize: 20,
                maxFiles: 1,
                method: "post",
                acceptedFiles: ".jpg,.jpeg,.png",
                paramName: "userfile",
                dictInvalidFileType: "Type file ini tidak dizinkan",
                addRemoveLinks: true,
                init: function () {
                    thisDropzone = this;
                    this.on("success", function (file, json) {
                        var obj = json;
                        $('.previews2').
                                append(
                                    "<input type='hidden' name='image2' value='" + obj + "'>\n\ "
                                        );

                    });
                } 
            });
          });


