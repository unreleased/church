// update admin profile validation
  $(document).ready(function(){      
      $("#adminProfile").validate({
        rules: {    
          username: {
            required: true
          },         
        }
    });
  })
   $('#adminProfile').on('submit', function(){
     $("#adminProfile").valid();
   });



// admin image upload by dropzone
  Dropzone.autoDiscover = false;
  $( document ).ready(function() { 
    var photo_upload = new Dropzone(".dropzone", {
        url: "AdminController/saveImage",
        maxFilesize: 20,
        maxFiles: 1,
        method: "post",
        acceptedFiles: ".jpg,.jpeg,.png",
        paramName: "userfile",
        dictInvalidFileType: "Type file ini tidak dizinkan",
        addRemoveLinks: true,
        init: function () {
            thisDropzone = this;
            this.on("success", function (file, json) {
                var obj = json;
                $('.previews').
                        append(
                            "<input type='hidden' name='image' value='" + obj + "'>\n\
                           ");

            });
        } 
    });
  });


