//DateTime picker
$(document).ready(function(){
   $('.datepicker').datetimepicker({});
});


 // edit event dropzone
   Dropzone.autoDiscover = false;
    $( document ).ready(function() { 
      var photo_upload2 = new Dropzone("#edit_dropzone", {
          url: "AdminEventController/saveImage",
          maxFilesize: 50,
          maxFiles: 50,
          method: "post",
          acceptedFiles: ".jpg,.jpeg,.png",
          paramName: "userfile",
          dictInvalidFileType: "Type file ini tidak dizinkan",
          addRemoveLinks: true,
          init: function () {
              thisDropzone = this;
              this.on("success", function (file, json) {
                  var obj = json;
                  $('.previews2').
                          append(
                              "<input type='hidden' name='image2[]' value='" + obj + "'>\n\
                            "
                                  );

              });
          } 
      });
    });


    // delete event image
     $(document).on('click', '.btn_delete_event_image', function(){         
         var id=$(this).data("event_image_id");
          if(confirm("Are you sure you want to delete this?"))  
          {  
            $.ajax({  
             url:"admin/delete-event-image",  
             method:"post",  
             data:{event_image_id:id},
             dataType:"text",  
             success:function(data){  
                      $("#"+id).fadeOut(); 
                    }  
            });  
          }  
        }); 

// edit form validation
  $(document).ready(function(){      
      $("#editEvent").validate({
        
    });
  })
   $('#editEvent').on('submit', function(){
     $("#editEvent").valid();
   });


//google map implementation
// $(document).ready(function(){
function editMap() {
    //var val="<?php echo $event_info[0]['event_google_map_longitude']; ?>";
    var latitude = parseFloat($("#latitude2").val()); 
    var longitude = parseFloat($("#longitude2").val()); 
    var event_location = $("#event_location").val(); 
    //alert(event_location);

       map = new google.maps.Map(document.getElementById('editMap'), {
            center: {lat: latitude, lng: longitude},
            zoom: 12
        });

       var marker = new google.maps.Marker({
            position: new google.maps.LatLng( latitude,longitude),
            map: map,
            title: event_location
        });

       var infowindow = new google.maps.InfoWindow({
          content: event_location
        });
        infowindow.open(map,marker);



        var input = document.getElementById('searchInput2');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29),
            draggable:true,
        });


          map.addListener('click',function(event) {
            marker.setPosition(event.latLng);
            document.getElementById('latitude2').value = event.latLng.lat();
            document.getElementById('longitude2').value = event.latLng.lng();
            infowindow.setContent('Latitude: ' + event.latLng.lat() + '<br>Longitude: ' + event.latLng.lng());
            infowindow.open(map,marker);        
        });
    
        google.maps.event.addListener(marker,'dragend',function(event) {
            document.getElementById('latitude2').value = event.latLng.lat();
            document.getElementById('longitude2').value = event.latLng.lng();
            infowindow.setContent('Latitude: ' + event.latLng.lat() + '<br>Longitude: ' + event.latLng.lng());
            infowindow.open(map,marker);
        });




        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();

            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }

        // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
        /*
        marker.setIcon(({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
        }));
        */
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
            infowindow.open(map, marker);

        
            document.getElementById('latitude2').value = place.geometry.location.lat();
            document.getElementById('longitude2').value = place.geometry.location.lng();
            document.getElementById('location2').value = place.formatted_address;
        });


}
// });


// <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9x8mCn5-P8XUl59uGqwmmcU6Alt1qza8&libraries=places&callback=editMap" async defer></script>