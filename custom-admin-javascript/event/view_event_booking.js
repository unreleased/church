// datatable
 $(function () {
    $('#example1').DataTable()
   
  })


// view event booking item
 $('.view_event_booking_item').on('click', function(){
        var booking_key   = $(this).closest('tr').attr('id'); 
       $.ajax({
            url:'admin/render_view_booking',
            method:'POST',
            data:{booking_key:booking_key},

            success:function(data){
                var result = JSON.parse(data);
                $('#view_booking_div').html(result.view_booking_div);
            }
        })
    });



  // delete booking
$(document).on('click', '.btn_delete_booking', function(){         
         var booking_key=$(this).data("id1");
          if(confirm("Are you sure you want to delete this?"))  
          {  
            $.ajax({  
             url:"admin/delete-event-booking",  
             method:"post",  
             data:{booking_key:booking_key},
             dataType:"text",  
             success:function(data){  
                      $("#"+booking_key).fadeOut(); 
                    }  
            });  
          }  
        });