
// view event booking item
 $(document).on('click','.view_event_booking_item', function(){
        var booking_key   = $(this).attr('event-booking-key'); 
       $.ajax({
            url:'admin/render_view_booking',
            method:'POST',
            data:{booking_key:booking_key},

            success:function(data){
                var result = JSON.parse(data);
                $('#view_booking_div').html(result.view_booking_div);
            }
        })
    });



  // delete booking
$(document).on('click', '.btn_delete_booking', function(){         
         var booking_key=$(this).data("delete_booking");
         var x=$(this); 
          if(confirm("Are you sure you want to delete this?"))  
          {  
            $.ajax({  
             url:"admin/delete-event-booking",  
             method:"post",  
             data:{booking_key:booking_key},
             dataType:"text",  
             success:function(data){  
                      x.closest('tr').fadeOut(); 
                    }  
            });  
          }  
        });


// dataTable    
  document.addEventListener('DOMContentLoaded', function () { 
    var table;
      table = $('#event_booking_table').DataTable({

          processing: true, //Feature control the processing indicator.
          serverSide: true, //Feature control DataTables' server-side processing mode.
          //order: [], //Initial no order.
          // Load data for the table's content from an Ajax source
          ajax: {
              url: "admin/load-event-booking-table",
              type: "post",
              complete: function (res) {}
          },
          //Set column definition initialisation properties.
          columnDefs: [
              {
                  "targets": [-1], //last column
                  "orderable": false, //set not orderable
              },
          ],
          "ordering": false

      });
  });