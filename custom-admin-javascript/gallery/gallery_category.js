// delete gallery category
$(document).on('click', '.btn_delete_gallery_category', function(){         
         var id=$(this).data("delete_gallery_category");
         var x=$(this);
          if(confirm("Are you sure you want to delete this?"))  
          {  
            $.ajax({  
             url:"admin/delete-gallery",  
             method:"post",  
             data:{gallery_key:id},
             dataType:"text",  
             success:function(data){  
                       x.closest('tr').fadeOut(); 
                    }  
            });  
          }  
        }); 


// edit gallery modal
  $(document).on('click','.edit_gallery_category_item' ,function(){
        var gallery_id   = $(this).attr('gallery-category-key'); 
        var gallery_category_name= $(this).attr('gallery-category-name');
        $("#e_gallery_category_name").val(gallery_category_name);
        $("#e_gallery_category_key").val(gallery_id);
      
    });

 // add form validation
 $(document).ready(function(){      
      $("#addGalleryCategory").validate({
        rules: {    
          gallery_category_name: {
            required: true
          },
          
        }
    });
  })

  $('#addGalleryCategory').on('submit', function(){
     $("#addGalleryCategory").valid();
  });


  // edit form validation
   $(document).ready(function(){      
      $("#editGalleryCategory").validate({
        rules: {    
          gallery_category_name: {
            required: true
          },          
        }
    });
  })

  $('#editGalleryCategory').on('submit', function(){
     $("#editGalleryCategory").valid();
  });


//mass select
   jQuery('#master').on('click', function(e) {
	  if($(this).is(':checked',true))  
	  {
	    $(".sub_chk").prop('checked', true);  
	  }  
	  else  
	  {  
	    $(".sub_chk").prop('checked',false);  
	  }  
	});


   //mass delete
    $(document).on('click', '.delete_all', function(){         
         var allVals = [];  
          $(".sub_chk:checked").each(function() {  
              allVals.push($(this).attr('data-id'));
            });   
            if(allVals.length ==0) 
            {  
              alert("Please select atleast one row.");  
            }  
             else {  
      WRN_PROFILE_DELETE = "Are you sure you want to delete all the selected row?";  
      var check = confirm(WRN_PROFILE_DELETE);  
      if(check == true){  
              //for server side        
              var join_selected_values = allVals.join(",");        
              $.ajax({ 
                type: "POST",  
                url: "admin/mass-gallery-category-delete",  
                cache:false,  
                data: 'ids='+join_selected_values,  
                success: function(response)  
                {  
                  location.reload();
                }   
              });       

            }  
          }
      }); 



    // dataTable    
  document.addEventListener('DOMContentLoaded', function () { 
    var table;
      table = $('#galleryCategoryTable').DataTable({
          processing: true, //Feature control the processing indicator.
          serverSide: true, //Feature control DataTables' server-side processing mode.
          //order: [], //Initial no order.
          // Load data for the table's content from an Ajax source
          ajax: {
              url: "admin/load-gallery-category-table",
              type: "post",
              complete: function (res) {}
          },
          //Set column definition initialisation properties.
          columnDefs: [
              {
                  "targets": [-1], //last column
                  "orderable": false, //set not orderable
              },
          ],
          "ordering": false

      });
  });



