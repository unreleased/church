// add form validation
  $(document).ready(function(){      
      $("#addBlog").validate({ 
        rules: {    
          blog_title: {
            required: true
          }, 
          blog_feature_image: {
            required: true,
          },
          blog_description: {
            required: true
          },         
        }
      });
  })
   $('#addBlog').on('submit', function(){
     $("#addBlog").valid();
   });


     // instant view image when select image for upload
   function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#viewFeatureImage')
                        .attr('src', e.target.result)
                        .width(200)
                        
                };

                reader.readAsDataURL(input.files[0]);
            }
        }


   // dataTable    
  document.addEventListener('DOMContentLoaded', function () { 
    var table;
      table = $('#blogTable').DataTable({

          processing: true, //Feature control the processing indicator.
          serverSide: true, //Feature control DataTables' server-side processing mode.
          //order: [], //Initial no order.
          // Load data for the table's content from an Ajax source
          ajax: {
              url: "admin/load-blog-table",
              type: "post",
              complete: function (res) {}
          },
          //Set column definition initialisation properties.
          columnDefs: [
              {
                  "targets": [-1], //last column
                  "orderable": false, //set not orderable
              },
          ],
          "ordering": false

      });
  });


   // edit blog
 $(document).on('click','.edit_blog', function(){
        var blog_key   = $(this).attr('blog-key');
       $.ajax({
            url:'admin/render_edit_blog',
            method:'POST',
            data:{blog_key:blog_key},
            success:function(data){
               var result = JSON.parse(data);
               $('#edit_blog_div').html(result.edit_blog_div);
            }
        })
    });


 // delete Blog
$(document).on('click', '.btn_delete_blog', function(){         
         var id=$(this).data("delete_blog");
         var x=$(this); 
          if(confirm("Are you sure you want to delete this?"))  
          {  
            $.ajax({  
             url:"admin/delete-blog",  
             method:"post",  
             data:{blog_key:id},
             dataType:"text",  
             success:function(data){
                       x.closest('tr').fadeOut(); 
                    }  
            });  
          }  
        }); 




//mass select
   jQuery('#master').on('click', function(e) {
    if($(this).is(':checked',true))  
    {
      $(".sub_chk").prop('checked', true);  
    }  
    else  
    {  
      $(".sub_chk").prop('checked',false);  
    }  
  });


   //mass delete
    $(document).on('click', '.delete_all', function(){         
         var allVals = [];  
          $(".sub_chk:checked").each(function() {  
              allVals.push($(this).attr('data-id'));
            });   
            if(allVals.length ==0) 
            {  
              alert("Please select atleast one row.");  
            }  
             else {  
      WRN_PROFILE_DELETE = "Are you sure you want to delete all the selected row?";  
      var check = confirm(WRN_PROFILE_DELETE);  
      if(check == true){  
              //for server side        
              var join_selected_values = allVals.join(",");        
              $.ajax({ 
                type: "POST",  
                url: "admin/mass-blog-delete",  
                cache:false,  
                data: 'ids='+join_selected_values,  
                success: function(response)  
                {  
                  location.reload();
                }   
              });       

            }  
          }
      }); 