// add form validation
  $(document).ready(function(){      
      $("#addUser").validate({ 
       rules: {    
          user_email: {
            required: true
          }, 
               
        }       
    });
  })
   $('#addUser').on('submit', function(){
     $("#addUser").valid();
   });


// user image upload by dropzone
  Dropzone.autoDiscover = false;
  $( document ).ready(function() { 
    var photo_upload = new Dropzone(".dropzone", {
        url: "AdminUserController/saveImage",
        maxFilesize: 20,
        maxFiles: 1,
        method: "post",
        acceptedFiles: ".jpg,.jpeg,.png",
        paramName: "userfile",
        dictInvalidFileType: "Type file ini tidak dizinkan",
        addRemoveLinks: true,
        init: function () {
            thisDropzone = this;
            this.on("success", function (file, json) {
                var obj = json;
                $('.previews').
                        append(
                            "<input type='hidden' name='image' value='" + obj + "'>\n\
                             ");

            });
        } 
    });
  });


   // delete User
$(document).on('click', '.btn_delete_user', function(){         
   var user_key=$(this).data("delete_user");
   var x=$(this);
    if(confirm("Are you sure you want to delete this?"))  
    {  
      $.ajax({  
       url:"admin/delete-user",  
       method:"post",  
       data:{user_key:user_key},
       dataType:"text",  
       success:function(data){  
                x.closest('tr').fadeOut(); 
              }  
      });  
    }  
  });


// edit user
$(document).on('click','.edit_user' ,function(){
       var user_key   = $(this).attr('user-key');
       $.ajax({
            url:'admin/render-edit-user',
            method:'POST',
            data:{id:user_key},

            success:function(data){
                var page_data = JSON.parse(data);
                $('#edit_user_div').html(page_data.edit_user_div);
            }
        })
    });


// dataTable    
  document.addEventListener('DOMContentLoaded', function () {
    var table;
      table = $('#userTable').DataTable({
          processing: true, //Feature control the processing indicator.
          serverSide: true, //Feature control DataTables' server-side processing mode.
          //order: [], //Initial no order.
          // Load data for the table's content from an Ajax source
          ajax: {
              url: "admin/load-user-table",
              type: "post",
              complete: function (res) {}
          },
          //Set column definition initialisation properties.
          columnDefs: [
              {
                  "targets": [-1], //last column
                  "orderable": false, //set not orderable
              },
          ],
          "ordering": false

      });
  });