// testimonial image upload by dropzone
 Dropzone.autoDiscover = false;
  $( document ).ready(function() { 
    var photo_upload = new Dropzone(".dropzone", {
        url: "AdminTestimonialController/saveImage",
        maxFilesize: 20,
        maxFiles: 1,
        method: "post",
        acceptedFiles: ".jpg,.jpeg,.png",
        paramName: "userfile",
        dictInvalidFileType: "Type file ini tidak dizinkan",
        addRemoveLinks: true,
        init: function () {
            thisDropzone = this;
            this.on("success", function (file, json) {
                var obj = json;

                 $('.previews').
                        append(
                            "<input type='hidden' name='image' value='" + obj + "'>\n\
                           "
                                );

            });
        } 
    });
  });


// add form validation
  $(document).ready(function(){      
      $("#addTestimonial").validate({
        rules: {    
          testimonial_user_name: {
            required: true
          }, 
          testimonial_description: {
            required: true
          },      
        }
    });
  })
   $('#addTestimonial').on('submit', function(){
     $("#addTestimonial").valid();
   });


// delete testimonial
$(document).on('click', '.btn_delete_testimonial', function(){         
         var id=$(this).data("delete_testimonial");
         var x=$(this); 
          if(confirm("Are you sure you want to delete this?"))  
          {  
            $.ajax({  
             url:"admin/delete-testimonial",  
             method:"post",  
             data:{testimonial_key:id},
             dataType:"text",  
             success:function(data){
                      x.closest('tr').fadeOut(); 
                    }  
            });  
          }  
        });


// edit testimonial
$(document).on('click','.edit_testimonial', function(){
        var testimonial_id   = $(this).attr('testimonial-key');
       $.ajax({
            url:'admin/render-edit-testimonial',
            method:'POST',
            data:{id:testimonial_id},

            success:function(data){
                var page_data = JSON.parse(data);
                $('#edit_testimonial_div').html(page_data.edit_testimonial_div);
            }
        })
    });



//mass select
   jQuery('#master').on('click', function(e) {
    if($(this).is(':checked',true))  
    {
      $(".sub_chk").prop('checked', true);  
    }  
    else  
    {  
      $(".sub_chk").prop('checked',false);  
    }  
  });


   //mass delete
    $(document).on('click', '.delete_all', function(){         
         var allVals = [];  
          $(".sub_chk:checked").each(function() {  
              allVals.push($(this).attr('data-id'));
            });   
            if(allVals.length ==0) 
            {  
              alert("Please select atleast one row.");  
            }  
             else {  
      WRN_PROFILE_DELETE = "Are you sure you want to delete all the selected row?";  
      var check = confirm(WRN_PROFILE_DELETE);  
      if(check == true){  
        //for server side        
        var join_selected_values = allVals.join(",");        
        $.ajax({ 
          type: "POST",  
          url: "admin/mass-testimonial-delete",  
          cache:false,  
          data: 'ids='+join_selected_values,  
          success: function(response)  
          {  
            location.reload();
          }   
        });       

      }  
    }            
        }); 


     // dataTable    
  document.addEventListener('DOMContentLoaded', function () { 
    var table;
      table = $('#testimonial_table').DataTable({

          processing: true, //Feature control the processing indicator.
          serverSide: true, //Feature control DataTables' server-side processing mode.
          //order: [], //Initial no order.
          // Load data for the table's content from an Ajax source
          ajax: {
              url: "admin/load-testimonial-table",
              type: "post",
              complete: function (res) {}
          },
          //Set column definition initialisation properties.
          columnDefs: [
              {
                  "targets": [-1], //last column
                  "orderable": false, //set not orderable
              },
          ],
          "ordering": false

      });
  });