document.addEventListener("DOMContentLoaded",function(){





})
//------------------------------------------------------------------
var map;
function initMap() {
    $event_map = document.getElementById('event-map');
    event_lat = parseFloat($event_map.getAttribute("evt-lat"));
    event_lng = parseFloat($event_map.getAttribute("evt-lng"));
    event_zoom = parseInt($event_map.getAttribute("evt-zoom"));
    event_title = $event_map.getAttribute("evt-title");

    lat_lang = {lat: event_lat, lng: event_lng};

    console.log(event_lat);
    console.log(event_lng);
    console.log(event_zoom);
    map = new google.maps.Map($event_map, {
        center: lat_lang,
        zoom: event_zoom
    });

    var marker = new google.maps.Marker({
        position: lat_lang,
        map: map,
        title: event_title,
        label:event_title,
        animation: google.maps.Animation.DROP,

    });

    marker.addListener('click', toggleBounce);
    marker.addListener('mouseover', toggleBounce);

    function toggleBounce() {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }


}
//------------------------------------------------------------------