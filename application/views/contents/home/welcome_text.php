<section id="welcom_text">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="gradintwhite">
                    <img src="assets/img/w1.jpg" class="img-fluid"></div>
            </div>
            <div class="col pad50">
                <h2>OUR CHURCH</h2>
                <p class="small_titel">We Come to World for Serving & Sharing</p>
                <p>Building a website is, in many ways, an exercise of willpower. It's tempting to get distracted by the
                    bells and whistles of the design process, and forget all about creating compelling But it's that
                    last part that's crucial to making inbound marketing work for your business....</p>
                <a class="btn btndarkborder" href="#">Read More</a>
            </div>
        </div>
    </div>
</section>
