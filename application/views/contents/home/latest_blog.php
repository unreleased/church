<section id="latest_blog">
    <div class="container">
        <div class="site_heading pad50">
            <h2>BLOG</h2>
            <p class="small_titel">Our Church Blog Post</p>
        </div>
        <div class="blog_item">
            <div class="row">
                <div class="col">
                    <div class="gradintwhite">
                        <img src="assets/img/b1.jpg" class="img-fluid"></div>
                </div>
                <div class="col">
                    <div class="b_latest_cont">
                        <h3>The Heartfelt Petition To The Lord</h3>
                        <p> -- 15 March 2017</p>
                        <p>Vestibulum aliquam orci ut lorem sodales, vel fermentum augue consectetur. Morbi eget massa
                            sed mi auctor faucibus eu eget urna. Interdum et malesuada fames ac ante ipsum primis in
                            faucibus. In mattis faucibus sapien. Mauris consectetur massa at semper tincidunt.</p>

                        <a href="#" class="btn btndarkborder">Read more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>