<section id="home_event" class="gradintbluere  wow bounceInDown" data-wow-duration="1s" data-wow-delay="1s">
    <div class="container">
        <?php if (!empty($event)) { ?>
            <div class="row">
                <div class="col">
                    <h2> NEXT UPCOMING EVENT</h2>
                    <p><?= $event['event_name'] ?></p>
                    <ul class="countdown_rs"
                        event-starts="<?= !empty($event['event_starts']) ? date("m/d/Y H:i:s", strtotime($event['event_starts'])) : date("m/d/Y H:i:s") ?>">
                        <li>
                            <span class="days  h2">00</span>
                            <p class="text-center">days</p>
                        </li>
                        <li>
                            <span class="hours h2">00</span>
                            <p class="text-center">hours</p>
                        </li>
                        <li>
                            <span class="minutes h2">00</span>
                            <p class="text-center">minutes</p>
                        </li>
                        <li>
                            <span class="seconds h2">00</span>
                            <p class="text-center">seconds</p>
                        </li>
                    </ul>
                    <a class="btn btndark" href="event/<?= $event['event_key'] ?>">View Event</a>
                </div>
                <div class="col ">
                    <div class="gradintblue">
                        <img src="<?= $event['featured_image']['event_image_name_with_path']?>"
                             class="img-fluid"></div>
                </div>
            </div>
        <?php } ?>
    </div>
</section>
