<section id="banner">

    <div class="home-banner">

        <?php if (!empty($banners)) { ?>
            <?php foreach ($banners as $banner) { ?>
                <div class="home-banner-item">
                    <div class="banner_img gradintblue"><img
                                src="<?= $this->config->item('banner_source_path') . $banner['banner_image_name'] ?>"
                                class="img-fluid"></div>
                    <div class="banner_content">
                        <div class="container text-center">
                            <h1><?= $banner['banner_image_title'] ?></h1>
                            <p><?= $banner['banner_image_subtitle'] ?></p>
                            <?php if (!empty($banner['banner_anchors'])) { ?>
                                <?php foreach ($banner['banner_anchors'] as $banner_anchor) { ?>
                                    <a href="<?= base_url()?><?= $banner_anchor['banner_anchor_url'] ?>"
                                       class="btn btn<?= empty($banner_anchor['banner_anchor_class']) ? 'light' : $banner_anchor['banner_anchor_class'] ?>">
                                        <?= $banner_anchor['banner_anchor_text'] ?>
                                    </a>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>


    </div>


</section>


