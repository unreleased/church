<section id="gallery" class="gradintblue wow bounceInUp" data-wow-duration="1s" data-wow-delay="1s">
    <div class="container">
        <div class="site_heading pad50">

            <h2>GALLERY</h2>
            <p class="small_titel">See Our Shop</p>
        </div>

        <?php if (!empty($featured_gallery_images)) { ?>
            <div class="row">
                <div class="col">
                    <?php if (isset($featured_gallery_images[0]['image_name'])) { ?>
                        <?php if (!empty($featured_gallery_images[0]['image_name'])) { ?>
                            <a href="<?= $this->config->item('gallery_source_path') . $featured_gallery_images[0]['image_name'] ?>"
                               data-lightbox="roadtrip" data-title="test title"><img
                                        src="<?= $this->config->item('gallery_source_path') . $featured_gallery_images[0]['image_name'] ?>"
                                        class="img-fluid"></a>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="col ss_g_r">

                    <div class="row">
                        <?php if (isset($featured_gallery_images[1]['image_name'])) { ?>
                            <?php if (!empty($featured_gallery_images[1]['image_name'])) { ?>
                                <div class="col"><a href="assets/img/g2.jpg" data-lightbox="roadtrip"
                                                    data-title="test title"><img
                                                src="assets/img/g2.jpg" class="img-fluid"></a></div>
                            <?php } ?>
                        <?php } ?>
                        <?php if (isset($featured_gallery_images[2]['image_name'])) { ?>
                            <?php if (!empty($featured_gallery_images[2]['image_name'])) { ?>
                                <div class="col"><a href="assets/img/g3.jpg" data-lightbox="roadtrip"
                                                    data-title="test title"><img
                                                src="assets/img/g3.jpg" class="img-fluid"></a></div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="row">
                        <?php if (isset($featured_gallery_images[3]['image_name'])) { ?>
                            <?php if (!empty($featured_gallery_images[3]['image_name'])) { ?>
                                <div class="col"><a href="assets/img/g4.jpg" data-lightbox="roadtrip"
                                                    data-title="test title"><img
                                                src="assets/img/g4.jpg" class="img-fluid"></a></div>
                            <?php } ?>
                        <?php } ?>
                        <?php if (isset($featured_gallery_images[4]['image_name'])) { ?>
                            <?php if (!empty($featured_gallery_images[4]['image_name'])) { ?>
                                <div class="col"><a href="assets/img/g5.jpg" data-lightbox="roadtrip"
                                                    data-title="test title"><img
                                                src="assets/img/g5.jpg" class="img-fluid"></a></div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>


    </div>
</section>
