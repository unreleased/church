<section id="page_title" class="gradintblue">
    <div class="banner_img"><img src="<?= $this->config->item('public_asset_path') ?>img/banner.jpg" class="img-fluid"></div>
    <div class="banner_content">
        <div class="container text-center">
            <h1>Upcoming  Events</h1>
            <ul class="bradcumb">
                <li><a href="/">Home</a></li>
                <li> / </li>
                <li>Events</li>
            </ul>

        </div>
    </div>
</section>
<style>
    .ajax-load{

        background: #e1e1e1;

        padding: 10px 0px;

        width: 100%;

    }
</style>

<section id="main_contant">
    <div class="container">
        <div class="site_heading pad50">
            <h2>UPCOMING EVENTS</h2>
            <p class="small_titel">Our Church Events Post</p>
        </div>

        <div class="events_list" base-url = "<?= base_url()?>">


        </div>
        <div class="ajax-load text-center" style="display:none">

            <p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Loading More post</p>

        </div>


    </div>
</section>

<script type="text/javascript" src="<?= $this->config->item('public_custom_js_path') ?>event_list.js"></script>