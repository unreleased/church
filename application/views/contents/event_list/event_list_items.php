
<!--<pre>
    <?php /*print_r($events)*/?>
</pre>-->

<?php if (!empty($events)) { ?>
    <?php foreach ($events as $event) { ?>
        <?php
        $url = !empty($event['event_key']) ? base_url() . 'event/' . $event['event_key'] : '#';
        $reg_url = !empty($event['event_key']) ? base_url() . 'event/' . $event['event_key'] . '?do_register=yes' : '#';
        ?>
        <div class="events_item">
            <div class="row">
                <div class="col-3">
                    <a class="gradintwhite">
                        <img src="<?= $event['featured_image']['event_image_name_with_path'] ?>"
                             class="img-fluid">
                    </a>
                </div>
                <div class="col-5">
                    <h5>
                        <a href="<?= $url ?>"><?= !empty($event['event_name']) ? $event['event_name'] : '' ?></a>
                    </h5>
                    <p class="e_smallt"><i class="fa fa-clock-o" aria-hidden="true"></i>
                        <?= !empty($event['event_starts']) ? date("M d Y @ h:i:a", strtotime($event['event_starts'])) : 'Unavailable' ?>
                        -
                        <?= !empty($event['event_ends']) ? date("M d Y @ h:i:a", strtotime($event['event_ends'])) : 'Unavailable' ?>
                    </p>
                    <p><i class="fa fa-map-marker" aria-hidden="true"></i>
                        <?= !empty($event['event_location']) ? $event['event_location'] : '' ?>
                    </p>
                </div>
                <div class="col">
                    <div class="e_date">
                        <h1> <?= !empty($event['event_starts']) ? date("d", strtotime($event['event_starts'])) : '' ?></h1>
                        <p><?= !empty($event['event_starts']) ? date("M", strtotime($event['event_starts'])) : '' ?></p>
                    </div>
                </div>
                <div class="col">
                    <a href="<?= $reg_url ?>"
                       class="btn btndark">Register Now</a>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>

