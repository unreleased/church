<section id="page_title" class="gradintblue">
    <div class="banner_img"><img src="<?= $this->config->item('public_asset_path') ?>img/banner.jpg" class="img-fluid"></div>
    <div class="banner_content">
        <div class="container text-center">
            <h1>Upcoming  Events</h1>
            <ul class="bradcumb">
                <li><a href="/">Home</a></li>
                <li> / </li>
                <li>Events</li>
            </ul>

        </div>
    </div>
</section>
<style>
    .ajax-load{

        background: #e1e1e1;

        padding: 10px 0px;

        width: 100%;

    }
</style>

<section id="main_contant">
    <div class="container">
        <div class="site_heading pad50">
            <h2>Staffs</h2>
            <p class="small_titel">People who work for the Lord</p>
        </div>

        <?php $icons = array();
        $icons['facebook'] = "fa-facebook";
        $icons['twitter'] = "fa-twitter";
        $icons['google'] = "fa-google-plus";
        $icons['youtube'] = "fa-youtube-play";
        ?>

        <div class="ss_our_staffs">
            <?php if (!empty($staffs)) { ?>
                <div class="row">
                    <?php foreach ($staffs as $staff) { ?>
                        <div class="col-md-3">
                            <a href="<?= base_url()?>staff/<?=$staff['staff_key']?>" class="staff_item anchor_staff_item text-center">
                                <div class="gradintwhitere">
                                    <img src="<?= $staff['featured_image']['staff_image_name_with_path'] ?>"
                                         class="img-fluid">
                                </div>

                                <p><?= $staff['staff_name'] ?></p>
                                <p class="small_titel"><?= !empty($staff['staff_type'])?$staff['staff_type']['staff_type_name']:"" ?></p>
                                <?php if (!empty($staff['staff_social_links'])) { ?>
                                    <ul class="ss_social_staff">
                                        <?php foreach ($staff['staff_social_links'] as $a_staff_social_link) { ?>
                                            <?php $icon = "";
                                                if (array_key_exists($a_staff_social_link['staff_social_link_name'], $icons)) {
                                                    $icon = $icons[$a_staff_social_link['staff_social_link_name']];
                                                }
                                            ?>
                                            <li>
                                                <a href="//<?= $a_staff_social_link['staff_social_link_url'] ?>">
                                                    <i class="fa <?= $icon ?>"></i>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </a>
                        </div>
                    <?php } ?>
                </div>

            <?php } ?>

        </div>



    </div>
</section>

<script type="text/javascript" src="<?= $this->config->item('public_custom_js_path') ?>staff_list.js"></script>