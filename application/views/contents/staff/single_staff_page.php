<section id="page_title" class="gradintblue">
    <div class="banner_img"><img src="<?= $this->config->item('public_asset_path') ?>img/banner.jpg" class="img-fluid">
    </div>
    <div class="banner_content">
        <div class="container text-center">
            <h1><?= !empty($staff) ? $staff['staff_name'] : '' ?></h1>
            <ul class="bradcumb">
                <li><a href="<?= base_url() ?>">Home</a></li>
                <li> /</li>
                <li><a href="<?= base_url() ?>">staffs</a></li>
                <li><?= !empty($staff) ? $staff['staff_name'] : '' ?></li>
            </ul>

        </div>
    </div>
</section>

<?php $icons = array();
$icons['facebook'] = "fa-facebook";
$icons['twitter'] = "fa-twitter";
$icons['google'] = "fa-google-plus";
$icons['youtube'] = "fa-youtube-play";
?>

<?php if (!empty($staff)) { ?>
    <section id="main_contant">
        <div class="container">

            <div class="pad50">

                <div href="<?= base_url()?>staff/<?=$staff['staff_key']?>" class="staff_item text-center card">
                    <div class="gradintwhitere">
                        <img src="<?= $staff['featured_image']['staff_image_name_with_path'] ?>"
                             class="img-fluid">
                    </div>

                    <p><?= $staff['staff_name'] ?></p>
                    <p class="small_titel"><?= !empty($staff['staff_type'])?$staff['staff_type']['staff_type_name']:"" ?></p>
                    <hr>
                    <div class="card-body">
                        <p class="m-t-10 m-b-10">
                            <?=$staff['staff_description']?>
                        </p>

                        <?php if (!empty($staff['staff_social_links'])) { ?>
                            <ul class="ss_social_staff">
                                <?php foreach ($staff['staff_social_links'] as $a_staff_social_link) { ?>
                                    <?php $icon = "";
                                    if (array_key_exists($a_staff_social_link['staff_social_link_name'], $icons)) {
                                        $icon = $icons[$a_staff_social_link['staff_social_link_name']];
                                    }
                                    ?>
                                    <li>
                                        <a href="//<?= $a_staff_social_link['staff_social_link_url'] ?>">
                                            <i class="fa <?= $icon ?>"></i>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    </div>

                </div>

            </div>



        </div>
    </section>
<?php } ?>


<script type="text/javascript" src="<?= $this->config->item('public_custom_js_path') ?>single_staff.js"></script>