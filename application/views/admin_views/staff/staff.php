<style>
  .delete{
  color:white;
  background-color:rgb(231, 76, 60);
  text-align:center;
  margin-top:2px;
  font-weight:700;
  border-radius:5px;
  min-width:20px;
  cursor:pointer;
}

.add-one{
  color:green;
  font-weigth:bolder;
  cursor:pointer;
  margin-top:10px;
}

.add-one2{
  color:green;
  font-weigth:bolder;
  cursor:pointer;
  margin-top:10px;
}
</style>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Staff
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Staff</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header">
          <?php $permission = $this->permission->hasAccess(array('add_staff','view_staff','edit_staff','delete_staff')); ?>

          <?php if($permission['add_staff']==1){ ?>
          <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default"> Add Staff </button>
          <?php } ?>
          </div>

            <?php if($permission['view_staff']==1){ ?>
            <!-- /.box-header -->
            <div class="box-body">
               <table id="staff_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                 <thead>
                   <tr>                  
                      <th>SL</th>
                      <th>Image</th>
                      <th>Name</th>                  
                      <th>Type</th>                  
                      <th>Action</th>
                    </tr>
                 </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
            <?php } ?>


            <?php if($permission['add_staff']==1){ ?>
            <!-- add modal start -->
            <div class="modal fade" id="modal-default">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Staff</h4>
                  </div>
                  <div class="modal-body">
                     <form id="addStaff"  role="form" action="admin/staff" method="POST" enctype="multipart/form-data">
                        <div class="box-body"> 


                        <div class="row">
                            <div class="col-md-6">
                             <div class="form-group">
                              <label for="staff_name" class="col-4 col-form-label">Name<span class="text-danger">*</span></label>
                              <input type="text" name="staff_name" class="form-control"
                                                  placeholder="Enter Name">
                            </div>
                            </div>

                            <div class="col-md-3">
                              <div class="form-group">
                                <label for="staff_type_id" class="col-4 col-form-label">Role<span class="text-danger">*</span></label>
                                <select required="required" name ="staff_type_id" class="form-control select2" style="width: 100%;">
                                    <option value="">---</option>
                                    <?php foreach ($view_staff_type as $type) { ?>
                                    <option value="<?php echo $type['staff_type_id'] ?>"><?php echo $type['staff_type_name'] ?></option>
                                    <?php } ?>
                                  </select>
                              </div>                        
                            </div>
                            <div class="col-md-3">
                             <div class="form-group">
                              <label for="staff_order" class="col-4 col-form-label">Order<span class="text-danger">*</span></label>
                              <input type="number" min="0" name="staff_order" class="form-control" placeholder="Enter Order (Ex: 0)">
                            </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                             <div class="form-group">
                              <label for="email" class="col-4 col-form-label">Email<span class="text-danger">*</span></label>
                              <input type="email" name="email" class="form-control"
                                                  placeholder="Enter Email">
                            </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="password" class="col-4 col-form-label">Password<span class="text-danger">*</span></label>
                                <input type="password" name="password" class="form-control"
                                                    placeholder="Enter Password">
                              </div>                                                 
                            </div>                            
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                             <div class="form-group">
                              <label for="facebook" class="col-4 col-form-label">Facebook URL<span class="text-danger"></span></label>
                              <input type="text" name="facebook" class="form-control"
                                                  placeholder="Enter Facebook URL">
                            </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="twitter" class="col-4 col-form-label">Twitter URL<span class="text-danger"></span></label>
                                <input type="text" name="twitter" class="form-control"
                                                    placeholder="Enter Twitter URL">
                              </div>                                                 
                            </div>                            
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                             <div class="form-group">
                              <label for="Google" class="col-4 col-form-label">Google URL<span class="text-danger"></span></label>
                              <input type="text" name="google" class="form-control"
                                                  placeholder="Enter Google URL">
                            </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="youtube" class="col-4 col-form-label">Youtube URL<span class="text-danger"></span></label>
                                <input type="text" name="youtube" class="form-control"
                                                    placeholder="Enter Youtube URL">
                              </div>                                                 
                            </div>                            
                        </div>

                         <div class="row">
                            <div class="col-md-6">
                             <div class="form-group">
                               <label for="image_name" class="col-form-label">Image<span class="text-danger"></span></label>
                                      <div class="dropzone" >
                                              <div class="dz-message" >
                                                  <h3> Click Here to select images</h3>
                                              </div>
                                      </div>
                            </div>

                           <div class="previews" id="preview"></div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="staff_description" class="col-4 col-form-label">Description</label>
                                <textarea type="text" name="staff_description" class="form-control" placeholder="Enter Description" rows="7"></textarea>
                              </div>                       
                            </div>
                        </div>

                          <div class="row">
                            <div class="col-md-6">
                             
                            </div>

                            <div class="col-md-6">
                                                     
                            </div>
                        </div>



                        <!-- end new form 
                                                  

                         <hr><hr>

                          <!-- banner anchor area start -->

                          <div class="dynamic-element form-groupp" style="display: none;">
                            <div class="row">
                              <div class="col-md-5">
                                <div class="form-group">                                  
                                  <input type="text" name="social_link_name[]" class="form-control" placeholder="Social Link Name">
                                </div>
                              </div>

                              <div class="col-md-5">
                                <div class="form-group">                                  
                                  <input type="text" name="social_link_url[]" class="form-control" placeholder="Social Link URL">
                                </div>
                              </div>

                              <div class="col-md-1">
                                <p class="delete">x</p>
                              </div>
                          </div>
                          </div>

                          <div class="dynamic-anchor">
                            <!-- Dynamic element will be cloned here -->
                            <!-- You can call clone function once if you want it to show it a first element-->
                          </div>

                          <div class="col-md-12">
                            <!-- <p class="add-one">+ Add Social Link</p> -->
                          </div>

                          <!-- banner anchor area end -->

                          
                                               
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>

                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <?php } ?>


            <?php if($permission['edit_staff']==1){ ?>
            <!-- edit modal start -->
            <div class="modal fade" id="edit_staff">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Staff</h4>
                  </div>
                  <div class="modal-body">
                       <div id="edit_staff_div">
                       </div>
                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <?php } ?>





          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->


  
