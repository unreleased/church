
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Staff Type
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="admin/staff-type">Staff Type</a></li>
        <li class="active">Add Staff Type</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>

      <!-- Default box -->
      <div class="box">
        

            <!-- /.box-header -->
            <div class="box-body">
            <form id="addStaffType"  role="form" action="admin/staff-type" method="POST" enctype="multipart/form-data">              
              <div class="row">
                <div class="col-md-6">                  
                        <div class="box-body"> 
                          <div class="form-group">
                            <label for="staff_type_name" class="col-4 col-form-label">Staff Type Name<span class="text-danger">*</span></label>
                            <input type="text" required name="staff_type_name" class="form-control" placeholder="Staff Type Name" value="">
                          </div>                                               
                        </div>
                        <!-- /.box-body -->      
                </div>                
              </div><hr>
                <div class="box-body">
                 <div class="form-group">
                    <label for="staff_type_permission" class="col-4 col-form-label"> Permission<span class="text-danger"></span></label>
                  </div> 
                

                <!-- user permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_user" name="add_user"> Add User
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_user" name="view_user"> View User
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_user" name="edit_user"> Edit User
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_user" name="delete_user"> Delete User
                  </div>
                </div><br>

                <!-- Gallary permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_gallery" name="add_gallery"> Add Gallery
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_gallery" name="view_gallery"> View Gallery
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_gallery" name="edit_gallery"> Edit Gallery
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_gallery" name="delete_gallery"> Delete Gallery
                  </div>
                </div><br>

                <!-- Gallary Image permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_gallery_image" name="add_gallery_image"> Add Gallery Image
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_gallery_image" name="view_gallery_image"> View Gallery Image
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_gallery_image" name="edit_gallery_image"> Edit Gallery Image
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_gallery_image" name="delete_gallery_image"> Delete Gallery Image
                  </div>
                </div><br>

                <!-- sermon permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_sermon" name="add_sermon"> Add Sermon
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_sermon" name="view_sermon"> View Sermon
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_sermon" name="edit_sermon"> Edit Sermon
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_sermon" name="delete_sermon"> Delete Sermon
                  </div>
                </div><br>

                <!-- banner permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_banner" name="add_banner"> Add Banner
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_banner" name="view_banner"> View Banner
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_banner" name="edit_banner"> Edit Banner
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_banner" name="delete_banner"> Delete Banner
                  </div>
                </div><br>

                <!-- blog permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_blog" name="add_blog"> Add Blog
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_blog" name="view_blog"> View Blog
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_blog" name="edit_blog"> Edit Blog
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_blog" name="delete_blog"> Delete Blog
                  </div>
                </div><br>

                <!-- staff type permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_staff_type" name="add_staff_type"> Add Staff Type
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_staff_type" name="view_staff_type"> View Staff Type
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_staff_type" name="edit_staff_type"> Edit Staff Type
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_staff_type" name="delete_staff_type"> Delete Staff Type
                  </div>
                </div><br>

                <!-- staff permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_staff" name="add_staff_type"> Add Staff
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_staff" name="view_staff"> View Staff
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_staff" name="edit_staff"> Edit Staff
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_staff" name="delete_staff"> Delete Staff
                  </div>
                </div><br>

                 <!-- testimonial permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_testimonial" name="add_testimonial"> Add Testimonial
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_testimonial" name="view_testimonial"> View Testimonial
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_testimonial" name="edit_testimonial"> Edit Testimonial
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_testimonial" name="delete_testimonial"> Delete Testimonial
                  </div>
                </div><br>

                <!-- event permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_event" name="add_event"> Add Event
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_event" name="view_event"> View Event
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_event" name="edit_event"> Edit Event
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_event" name="delete_event"> Delete Event
                  </div>
                </div><br>

                 <!-- booking permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_booking" name="add_booking"> Add Booking
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_booking" name="view_booking"> View Booking
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_booking" name="edit_booking"> Edit Booking
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_booking" name="delete_booking"> Delete Booking
                  </div>
                </div><br>

                



                </div>

                <br><div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>                
            </form>

            </div>
            <!-- /.box-body -->
            
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->

  <script>
  $(function () {
    $('#example1').DataTable()
   
  })
</script>










  
