

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Page
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header">
           <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add_page"> Add Page </button>
          </div>

           
            <!-- /.box-header -->
            <div class="box-body">
              <table id="galleryImage" class="table table-striped table-bordered" cellspacing="0" width="100%">
                 <thead>
                  <tr>
                    <th>SL</th>
                    <th>Heading</th>
                    <th>Slug</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    <?php $sl=0; foreach ($view_page as $page) { $sl++;?>
                   <tr id="<?php echo $page['page_key']; ?>">
                        <td><?php echo $sl; ?></td>
                        <td><?php echo $page['page_title']; ?></td>
                        <td><?php echo $page['page_slug']; ?></td>
                        <td>
                          <?php
                            if($page['page_status']==1){
                              echo "Enable";
                            }
                            if($page['page_status']==0){
                              echo "Disable";
                            }
                          ?>
                          </td>
                        <td class="modalOpen">
                            <a data-toggle="modal" class="edit_page" data-target="#edit_page" href="javascript:void(0)"><i style="margin-right:10px; font-size: 16px;" data-toggle="tooltip" title="Edit" class="fa fa-pencil"></i></a>

                             <a href="javascript:void(0)"><i data-delete_page="<?php echo $page['page_key']; ?>" style="font-size: 16px;" data-toggle="tooltip" title="Delete" class="fa fa-trash btn_delete_page"></i></a>
                        </td>
                    </tr>
                   <?php } ?> 
                   
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->


            <!-- add modal start -->
            <div class="modal fade" id="add_page">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Page</h4>
                  </div>
                  <div class="modal-body">
                     <form id="addPage"  role="form" action="admin/page" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                          <div class="row">
                            <div class="col-md-8">
                              <div class="form-group">
                                <label for="blog_title" class="col-form-label">Page Title<span class="text-danger">*</span></label>
                                <input type="text" name="page_title" class="form-control" required="required" placeholder="Page Title">
                              </div>

                              <div class="form-group">
                                <label for="page_slug" class="col-form-label">Page Slug<span class="text-danger">*</span></label>
                                <input type="text" name="page_slug" class="form-control" required="required" placeholder="Page Slug">
                              </div>  

                              <div class="form-group">
                                <label for="page_header_image" class="col-form-label">Page Header Image<span class="text-danger">*</span></label>
                                <input type="file" onchange="readURL(this);" name="page_header_image" class="form-control" required="required" accept=".png, .jpg, .jpeg" placeholder="Page Header Image" >
                              </div>
                            </div>

                            <div class="col-md-4"> <br><br>                                
                              <img id="viewFeatureImage" src="<?php echo $this->config->item('default_image_placeholder') ?>" style="height: 150px; width: 220px;" alt="your image" />
                            </div>
                          </div>                         

                        <div class="form-group">
                          <label for="page_content" class="col-4 col-form-label">Page Content<span class="text-danger"></span></label>
                          <textarea id="page_content" name="page_content" class="form-control" placeholder="Page Content"></textarea>
                        </div>
                                     
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>
                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            
              <!-- edit modal start -->
            <div class="modal fade" id="edit_page">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Page</h4>
                  </div>
                  <div class="modal-body">
                       <div id="edit_page_div">
                       </div>
                  </div>                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            


          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->

    <script type="text/javascript">
     
    </script>
