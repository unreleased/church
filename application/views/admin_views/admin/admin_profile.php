<?php //print_r($admin_info); die; ?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admin Profile
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Admin Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>

      <!-- Default box -->
      <div class="box">
        

            <!-- /.box-header -->
            <div class="box-body">
              
              <div class="row">
                <div class="col-md-6">
                  <form id="adminProfile"  role="form" action="admin/admin-profile" method="POST" enctype="multipart/form-data">
                        <div class="box-body">

                          <div class="form-group">
                             <label for="image_name" class="col-form-label">Image<span class="text-danger"></span></label>
                                    <div class="dropzone" >
                                            <div class="dz-message" >
                                                <h3> Click Here to select images</h3>
                                            </div>
                                    </div>
                          </div>
                          <div class="previews" id="preview"></div>
                          

                          <div class="form-group">
                            <label for="email" class="col-4 col-form-label">Email<span class="text-danger">*</span></label>
                            <input type="text" required class="form-control" disabled="disabled" placeholder="Enter Email" value="<?php echo $admin_info[0]['email']; ?>">
                          </div>

                          <div class="form-group">
                            <label for="username" class="col-4 col-form-label">Username<span class="text-danger">*</span></label>
                            <input type="text" required name="username" class="form-control" placeholder="Enter Username" value="<?php echo $admin_info[0]['username']; ?>">
                          </div>

                          <div class="form-group">
                            <label for="password" class="col-4 col-form-label">Password<span class="text-danger"></span></label>
                            <input type="password" name="password" class="form-control"
                                                placeholder="******">
                          </div>

                           <input type="hidden" name="admin_key" class="form-control"
                                                value="<?php echo $admin_info[0]['admin_key']; ?>">
                                               
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>
                </div>

                <div class="col-md-6"><br><br>
                  <p>
                      <?php 
                          if(!empty($admin_info[0]['image'])){ ?>                                  
                               <img src="<?php echo $this->config->item('admin_source_path').$admin_info[0]['image']; ?>" style="height: 150px; width: 150px;"> 
                         <?php } ?>

                  </p>
                </div>
              </div>

            </div>
            <!-- /.box-body -->
            
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->

  <script>
  $(function () {
    $('#example1').DataTable()
   
  })
</script>










  
