<form id="editGallery"  role="form" action="admin/update-gallery-item" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                          

                          <div class="row">
                            <div class="col-md-8">                            

                          <div class="form-group">
                             <label for="image_name" class="col-form-label">Image<span class="text-danger">*</span></label>
                                    <div class="dropzone" id="edit_dropzone">
                                        <div class="dz-message" >
                                            <h3> Click Here to select images</h3>
                                        </div>
                                    </div>
                          </div>

                           <div class="previews2" id="preview2"></div>

                            </div>

                            <div class="col-md-4" style="margin-top: 5%;">
                              
                              <img id="blah" src="<?php echo $this->config->item('gallery_source_path').$itemInfo[0]['image_name']; ?>" style="height: 100px; width: 100px;">
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="gallery_category_id" class="col-4 col-form-label">Gallery<span class="text-danger">*</span></label>
                            <select required="required" name ="gallery_category_id" class="form-control select2" style="width: 100%;">
                                <?php foreach ($view_gallery_category as $category) { ?>
                                <option value="<?php echo $category['gallery_category_id'] ?>" <?php if($itemInfo[0]['gallery_category_id']==$category['gallery_category_id']){echo "selected";} ?>><?php echo $category['gallery_category_name'] ?></option>
                                <?php } ?>
                            </select>
                          </div>  

                          <div class="form-group">
                            <label for="is_featured_image" class="col-4 col-form-label">Feature Image<span class="text-danger">*</span></label>
                            <select required="required" name ="is_featured_image" class="form-control select2" style="width: 100%;">
                                <option value="0" <?php if($itemInfo[0]['is_featured_image']==0){echo "selected";} ?>>NO</option>
                                <option value="1" <?php if($itemInfo[0]['is_featured_image']==1){echo "selected";} ?>>YES</option>
                              </select>
                          </div>

                          <div class="form-group">
                            <label for="image_order" class="col-4 col-form-label">Order<span class="text-danger">*</span></label>
                            <input required="required" type="number" min="0" value="<?php echo $itemInfo[0]['image_order']; ?>" name="image_order" class="form-control" placeholder="Enter Order (Ex: 0)">
                          </div>

                          <input type="hidden" value="<?php echo $itemInfo[0]['image_key']; ?>" name="image_key" class="form-control" >

                          <div class="form-group">
                            <label for="image_description" class="col-4 col-form-label">Description<span class="text-danger">*</span></label>
                            <input required="required" type="text" value="<?php echo $itemInfo[0]['image_description']; ?>" name="image_description" class="form-control"
                                                placeholder="Enter Description">
                          </div>
                                               
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                          <button id="editGallery" type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>

                      <script  type="text/javascript" src="custom-admin-javascript/gallery/gallery_edit.js"></script>