
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header">
           
      <?php $result = $this->permission->hasAccess(array('add_user','view_user','edit_user','delete_user')); ?>

          <?php if($result['add_user']==1){ ?>
           <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addUser"> Add User</button>
          <?php } ?>

          </div>

          <?php if($result['view_user']==1){ ?>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="userTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>                                
                    <th>SL</th>
                    <th>Photo</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
            <?php } ?>

            <?php if($result['add_user']==1){ ?>
            <!-- add modal start -->
            <div class="modal fade" id="addUser">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add User</h4>
                  </div>
                  <div class="modal-body">
                     <form id="addUser"  role="form" action="admin/user" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                          <div class="form-group">
                             <label for="image_name" class="col-form-label">Image<span class="text-danger"></span></label>
                                    <div class="dropzone" >
                                            <div class="dz-message" >
                                                <h3> Click Here to select images</h3>
                                            </div>
                                    </div>
                          </div>
                          <div class="previews" id="preview"></div>
                          

                          <div class="form-group">
                            <label for="user_email" class="col-4 col-form-label">Email<span class="text-danger">*</span></label>
                            <input type="email" required name="user_email" class="form-control" placeholder="Enter Email">
                          </div>

                          <div class="form-group">
                            <label for="user_username" class="col-4 col-form-label">Username<span class="text-danger">*</span></label>
                            <input type="text" required name="user_username" class="form-control" placeholder="Enter Username">
                          </div>

                          <div class="form-group">
                            <label for="user_role" class="col-4 col-form-label">User Role<span class="text-danger">*</span></label>
                            
                            <select required="required" name ="user_role" class="form-control select2" style="width: 100%;">
                                <option value="">---</option>
                                <?php foreach ($all_role as $role) { ?>
                                <option value="<?php echo $role['user_role_id'] ?>"><?php echo $role['user_role_name'] ?></option>
                                <?php } ?>
                              </select>

                          </div>

                          <div class="form-group">
                            <label for="user_phone" class="col-4 col-form-label">Phone<span class="text-danger">*</span></label>
                            <input type="number" required name="user_phone" class="form-control" placeholder="Enter User Phone">
                          </div>

                          <div class="form-group">
                            <label for="user_password" class="col-4 col-form-label">Password<span class="text-danger">*</span></label>
                            <input type="password" minlength="6" required name="user_password" class="form-control" placeholder="Enter Password">
                          </div>
                                               
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>

                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <?php } ?>


         <?php if($result['edit_user']==1){ ?>
          <!-- edit modal start -->
            <div class="modal fade" id="exampleModal2">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit User</h4>
                  </div>
                  <div class="modal-body">
                       <div id="edit_user_div">
                       </div>
                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <?php } ?>





          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->

  









  
