    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Blog Comment
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="admin/blog">Blog</a></li>
        <li class="active">Blog Comment</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header">
          </div>

            <!-- /.box-header -->
            <div class="box-body">
              <table id="blogCommentTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                 <thead>
                  <tr>
                    <th>SL</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Message</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                   <?php $sl=0; foreach ($blog_comment as $comment) { $sl++;?>
                   <tr> 
                        <td><?php echo $sl; ?></td>                                            
                        <td><?php echo $comment['blog_comment_username']; ?></td>
                        <td><?php echo $comment['blog_comment_useremail']; ?></td>
                        <td> 
                          <?php 
                            $count = strlen($comment['blog_comment_usermessage']);
                            if($count>50){ $more = "...";}
                            else{$more='';}
                            $message = substr($comment['blog_comment_usermessage'], 0, 50);
                            echo $message.$more;
                          ?>
                        </td>
                        <td>
                          <?php 
                           if($comment['blog_comment_status']==1){ ?>
                               <span class="btn btn-success"> Approved </span>
                            <?php } ?>
                            <?php if($comment['blog_comment_status']==0){ ?>
                               <span class="btn btn-danger"> Unapproved </span>
                            <?php } ?>  
                        </td>
                        <td class="modalOpen"> 
                            <a data-toggle="modal" blog_comment_key="<?php echo $comment['blog_comment_key'] ?>" class="view_comment" data-target="#view_comment" href="javascript:void(0)"><i style="margin-right:10px; font-size: 16px;" data-toggle="tooltip" title="Edit" class="fa fa-eye"></i></a>

                            <a href="javascript:void(0)"><i data-delete_comment="<?php echo $comment['blog_comment_key']; ?>" style="font-size: 16px;" data-toggle="tooltip" title="Delete" class="fa fa-trash btn_delete_blog_comment"></i></a>
                        </td>
                    </tr>
                   <?php } ?> 



                </tbody>
              </table>
            </div>
            <!-- /.box-body -->

            

              <!-- edit modal start -->
            <div class="modal fade" id="view_comment">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">View Comment</h4>
                  </div>
                  <div class="modal-body">
                       <div id="edit_comment_div">
                       </div>
                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->
