    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Blog
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Blog</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header">
          <?php $permission = $this->permission->hasAccess(array('add_blog','view_blog','edit_blog','delete_blog')); ?>

           <?php if($permission['add_blog']==1){ ?>  
           <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add_post"> Add Post </button>
           <?php } ?>
          <?php if($permission['delete_blog']==1){ ?>   
           <span class="delete_all btn btn-danger"> Delete All </span>
           <?php } ?>
          </div>

           <?php if($permission['view_blog']==1){ ?>   
            <!-- /.box-header -->
            <div class="box-body">
              <table id="blogTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                 <thead>
                  <tr>
                    <th>
                      <input type="checkbox" id="master">
                    </th>
                    <th>SL</th>
                    <th>Title</th>
                    <th>Comment</th>
                    <th>Media</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
            <?php } ?>

            <?php if($permission['add_blog']==1){ ?>  
            <!-- add modal start -->
            <div class="modal fade" id="add_post">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Post</h4>
                  </div>
                  <div class="modal-body">
                     <form id="addBlog"  role="form" action="admin/blog" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                          <div class="row">
                            <div class="col-md-8">
                              <div class="form-group">
                                <label for="blog_title" class="col-form-label">Blog Title<span class="text-danger">*</span></label>
                                <input type="text" name="blog_title" class="form-control" required="required" placeholder="Blog Title">
                              </div>  

                              <div class="form-group">
                                <label for="blog_feature_image" class="col-form-label">Feature Image<span class="text-danger">*</span></label>
                                <input type="file" onchange="readURL(this);" name="blog_feature_image" class="form-control" required="required" accept=".png, .jpg, .jpeg" placeholder="Blog Feature Image" >
                              </div>
                            </div>

                            <div class="col-md-4">                              
                              <img id="viewFeatureImage" src="<?php echo $this->config->item('default_image_placeholder') ?>" style="height: 150px; width: 220px;" alt="your image" />
                            </div>
                          </div>                         

                        <div class="form-group">
                          <label for="blog_description" class="col-4 col-form-label">Description<span class="text-danger">*</span></label>
                          <textarea id="blog_description" name="blog_description" class="form-control" placeholder="Enter Description"></textarea>
                        </div>

                        <div class="form-group">
                          <label for="blog_tag" class="col-4 col-form-label">Tag<span class="text-danger"></span></label>
                         <select name="blog_tag[]" class="form-control select2" multiple="multiple" data-placeholder="Enter Tag" style="width: 100%;"> 
                                </select>
                        </div>
                                               
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>
                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <?php } ?>


            <?php if($permission['edit_blog']==1){ ?>  
              <!-- edit modal start -->
            <div class="modal fade" id="edit_blog">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Blog</h4>
                  </div>
                  <div class="modal-body">
                       <div id="edit_blog_div">
                       </div>
                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <?php } ?>
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->

    <script type="text/javascript">
      $(document).ready(function () {
            CKEDITOR.replace('blog_description');
    });
    </script>

    <script type="text/javascript">
      $(document).ready(function(){
          $('.select2').select2({
            tags: true,
              tokenSeparators: [',', ' ']
          })
    }); 
    </script>
