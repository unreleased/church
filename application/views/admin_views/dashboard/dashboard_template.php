<base href="<?php echo base_url();?>">
<!DOCTYPE html>
<html>
	<head>
		<?php echo $headerlink;?>
	</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
	<?php echo $header;?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">   
    <!-- sidebar -->
	<?php echo $sidebar;?>	
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	<?php echo $content;?>    
  </div>
  <!-- /.content-wrapper -->
  
   <!--footer -->
  <?php echo $footer;?>   
</body>
</html>
