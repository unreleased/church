<style>
  #map{
    height: 200px;
}

#editMap{
    height: 200px;
}

.pac-container{
    z-index: 10000000000;
}
</style>



    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Event
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Event</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header">
          <?php $permission = $this->permission->hasAccess(array('add_event','view_event','edit_event','delete_event')); ?>

          <?php if($permission['add_event']==1){ ?> 
           <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default"> Add Event </button>
           <?php } ?>

           <?php if($permission['delete_event']==1){ ?> 
           <span class="delete_all btn btn-danger"> Delete All </span>
           <?php } ?>
          </div>

            <?php if($permission['view_event']==1){ ?> 
            <!-- /.box-header -->
            <div class="box-body">
              <table id="event_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                 <thead>
                    <tr>
                      <th>
                        <input type="checkbox" id="master">
                      </th>
                      <th>SL</th>
                      <th>Title</th>
                      <th>Seat</th>
                      <th>Media</th>
                      <th>Action</th>
                    </tr>
                 </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
            <?php } ?>


            <?php if($permission['add_event']==1){ ?> 
            <!-- add modal start -->
            <div class="modal fade" id="modal-default">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Event</h4>
                  </div>
                  <div class="modal-body">
                     <form id="addEvent"  role="form" action="admin/event" method="POST" enctype="multipart/form-data" autocomplete="off">
                        <div class="box-body">
                           <div class="row">

                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="event_name" class="col-form-label">Event Name<span class="text-danger"></span></label>
                                <input type="text" name="event_name" class="form-control" required="required" placeholder="Event Name">
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="event_location" class="col-form-label">Event Location<span class="text-danger"></span></label>
                                <input type="text" name="event_location" class="form-control" required="required" placeholder="Event Location"> 
                              </div>                            
                            </div>
                        </div>

                         <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="event_start_date_time" class="col-form-label">Event Start Date Time<span class="text-danger"></span></label>
                                <input type="text" name="event_start_date" class="form-control datepicker" id="datepicker" required="required" placeholder="Event Start Date">
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="event_end_date_time" class="col-form-label">Event End Date Time<span class="text-danger"></span></label>
                                <input type="text" name="event_end_date" class="form-control datepicker" placeholder="Event End Date">
                              </div>
                            </div>

                        </div>

                         <div class="form-group">
                          <label for="event_description" class="col-form-label">Event Description<span class="text-danger"></span></label>
                            <textarea type="text" name="event_description" class="form-control" placeholder="Event Description"></textarea>
                          </div>

                        

                        <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="event_phone" class="col-form-label">Event Phone<span class="text-danger"></span></label>
                                <input type="text" name="event_phone" class="form-control" placeholder="Event Phone">
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="event_website" class="col-form-label">Event Website<span class="text-danger"></span></label>
                                <input type="text" name="event_website" class="form-control" placeholder="Event Website"> 
                              </div>                            
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="event_email" class="col-form-label">Event Email<span class="text-danger"></span></label>
                                <input type="email" name="event_email" class="form-control" placeholder="Event Email"> 
                              </div>                            
                            </div>
                          </div>

                        <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="event_seat" class="col-form-label">Event Seat<span class="text-danger"></span></label>
                                <input type="number" min="0" name="event_seat" class="form-control" required="required" placeholder="Number of Seat"> 
                              </div>                            
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="is_event_featured" class="col-form-label">Event Featured?<span class="text-danger"></span></label>
                               
                               <select required="required" name="is_event_featured" class="form-control select2 valid" style="width: 100%;" aria-invalid="false">                                
                                  <option value="1">YES</option>
                                  <option value="0">NO</option>
                              </select>

                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="is_event_booking_enabled" class="col-form-label">Booking Enable?<span class="text-danger"></span></label>
                               
                               <select required="required" name="is_event_booking_enable" class="form-control select2 valid" style="width: 100%;" aria-invalid="false">                                
                                  <option value="1">YES</option>
                                  <option value="0">NO</option>
                              </select>

                              </div>
                            </div>

                            </div>
                          </div>

                        <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <input id="latitude" type="hidden" name="latitude" class="form-control" placeholder="Latitude">
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <input id="longitude" type="hidden" name="longitude" class="form-control" placeholder="Longitude"> 
                              </div>                            
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <input type="hidden" id="location" name="location" class="form-control" required="required" placeholder="Location">
                              </div>                            
                            </div>
                        </div>


                         <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                 <label for="event_email" class="col-form-label">Event Image<span class="text-danger"></span></label>
                                    <div class="dropzone" >
                                            <div class="dz-message" >
                                                <h3> Click Here to select images</h3>
                                            </div>
                                    </div>
                              </div>
                              <div class="previews" id="preview"></div>
                            </div>

                            <div class="col-md-6">
                                <article class="post property-item" >
                                   <label for="event_email" class="col-form-label">Set Map Location<span class="text-danger"></span></label>
                                  <input id="searchInput" class="form-control" type="text" placeholder="Select Event Location">
                                  
                                  <div id="map"></div>
                               </article>
                            </div>

                           
                        </div>




                        <!-- /.box-body -->

                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>

                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <?php } ?>


            <?php if($permission['edit_event']==1){ ?> 
            <!-- edit modal start -->
            <div class="modal fade" id="exampleModal2">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Event</h4>
                  </div>
                  <div class="modal-body">
                       <div id="edit_event_div">
                       </div>
                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <?php } ?>





          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->

  <script>
  $(function () {
    $('#example1').DataTable()
  })
</script> 




<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9x8mCn5-P8XUl59uGqwmmcU6Alt1qza8&libraries=places&callback=initMap" async defer></script>







