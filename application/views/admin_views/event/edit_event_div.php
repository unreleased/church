<form id="editEvent"  role="form" action="admin/update-event" method="POST" enctype="multipart/form-data" autocomplete="off">
                        <div class="box-body">
                           <div class="row">

                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="event_name" class="col-form-label">Event Name<span class="text-danger"></span></label>
                                <input type="text" name="event_name" class="form-control" required="required" placeholder="Event Name" value="<?php echo $event_info[0]['event_name'] ?>">
                              </div>
                            </div>

                            <input type="hidden" name="event_id" class="form-control" value="<?php echo $event_info[0]['event_id'] ?>" >

                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="event_location" class="col-form-label">Event Location<span class="text-danger"></span></label>
                                <input id="event_location" type="text" name="event_location" class="form-control" required="required" placeholder="Event Location" value="<?php echo $event_info[0]['event_location'] ?>"> 
                              </div>                            
                            </div>
                        </div>

                         <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">

                                <?php 
                                if(!empty($event_info[0]['event_ends'])){
                                  $dbStartTime = $event_info[0]['event_starts'];
                                  $startTime = date('m/d/Y h:i A',strtotime($dbStartTime));
                                }
                                else{$startTime='';}
                                ?>
                                <label for="event_start_date_time" class="col-form-label">Event Start Date Time<span class="text-danger"></span></label>
                                <input type="text" name="event_start_date" class="form-control datepicker" id="datepicker" required="required" placeholder="Event Start Date" value="<?php echo $startTime; ?>">
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <?php 
                                if(!empty($event_info[0]['event_ends'])){
                                  $dbEndTime = $event_info[0]['event_ends'];
                                  $endTime = date('m/d/Y h:i A',strtotime($dbEndTime));
                                }
                                else{$endTime='';}
                                ?>
                                <label for="event_end_date_time" class="col-form-label">Event End Date Time<span class="text-danger"></span></label>
                                <input type="text" name="event_end_date" class="form-control datepicker" placeholder="Event End Date" value="<?php echo $endTime; ?>">
                              </div>
                            </div>

                        </div>

                         <div class="form-group">
                          <label for="event_description" class="col-form-label">Event Description<span class="text-danger"></span></label>
                            <textarea type="text" name="event_description" class="form-control" placeholder="Event Description"><?php echo $event_info[0]['event_description'] ?></textarea>
                          </div>

                        

                        <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="event_phone" class="col-form-label">Event Phone<span class="text-danger"></span></label>
                                <input type="text" name="event_phone" class="form-control" placeholder="Event Phone" value="<?php echo $event_info[0]['event_phone'] ?>">
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="event_website" class="col-form-label">Event Website<span class="text-danger"></span></label>
                                <input type="text" name="event_website" class="form-control" placeholder="Event Website" value="<?php echo $event_info[0]['event_website'] ?>"> 
                              </div>                            
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="event_email" class="col-form-label">Event Email<span class="text-danger"></span></label>
                                <input type="email" name="event_email" class="form-control" placeholder="Event Email" value="<?php echo $event_info[0]['event_email'] ?>"> 
                              </div>                            
                            </div>
                          </div>

                        <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="event_seat" class="col-form-label">Event Seat<span class="text-danger"></span></label>
                                <input type="number" min="0" name="event_seat" class="form-control" required="required" placeholder="Number of Seat" value="<?php echo $event_info[0]['event_seat'] ?>"> 
                              </div>                            
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="is_event_featured" class="col-form-label">Event Featured?<span class="text-danger"></span></label>
                               
                               <select required="required" name="is_event_featured" class="form-control select2 valid" style="width: 100%;" aria-invalid="false">                                
                                  <option value="1" <?php if($event_info[0]['is_event_featured']==1){echo "selected";} ?>>YES</option>
                                  <option value="0" <?php if($event_info[0]['is_event_featured']==0){echo "selected";} ?>>NO</option>
                              </select>

                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="is_event_booking_enabled" class="col-form-label">Booking Enable?<span class="text-danger"></span></label>
                               
                               <select required="required" name="is_event_booking_enable" class="form-control select2 valid" style="width: 100%;" aria-invalid="false">                                
                                  <option value="1" <?php if($event_info[0]['is_event_booking_enabled']==1){echo "selected";} ?>>YES</option>
                                  <option value="0" <?php if($event_info[0]['is_event_booking_enabled']==0){echo "selected";} ?>>NO</option>
                              </select>

                              </div>
                            </div>

                            </div>
                          </div>

                        <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <input id="latitude2" type="hidden" name="latitude" class="form-control" placeholder="Latitude" value="<?php echo $event_info[0]['event_google_map_latitude'] ?>">
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <input id="longitude2" type="hidden" name="longitude" class="form-control" placeholder="Longitude" value="<?php echo $event_info[0]['event_google_map_longitude'] ?>"> 
                              </div>                            
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <input type="hidden" id="location2" name="location" class="form-control"  placeholder="Location" value="<?php echo $event_info[0]['event_google_map_location'] ?>">
                              </div>                            
                            </div>
                        </div>


                         <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                 <label for="image_name" class="col-form-label">Event Image<span class="text-danger"></span></label>
                                        <div class="dropzone" id="edit_dropzone">
                                            <div class="dz-message" >
                                                <h3> Click Here to select images</h3>
                                            </div>
                                        </div>
                              </div>

                               <div class="previews2" id="preview2"></div>


                              <?php foreach ($view_event_image as $image) { ?>

                                <span id="<?php echo $image['event_image_id']; ?>" class="editEventImgDiv" style="margin-right: 10px;">
                                  <input type="hidden" name="image2[]" value="<?php echo $image['event_image_name']; ?>" > 

                                  <img class="card-img-top img-fluid footage_thumb" style="height:60px; width:60px;" src="<?php echo $this->config->item('event_source_path').$image['event_image_name']; ?>"  alt="Event Image">

                                  <input type="radio" id="event_feature_image" name="event_feature_image" value="<?php echo $image['event_image_name']; ?>" data-toggle="tooltip" title="Set Feature Image" <?php if($image['is_event_image_featured']=='1'){echo "checked";} ?> data-parsley-multiple="add_event">
                                  
                                  <span><a href="void:javascript(0);" data-toggle="tooltip" title="Delete Image" data-event_image_id="<?php echo $image['event_image_id']; ?>" class="btn_delete_event_image">X</a></span>
                                </span>
                                <?php } ?>

                            </div>

                            <div class="col-md-6">
                                <article class="post property-item" >
                                   <label for="event_email" class="col-form-label">Set Map Location<span class="text-danger"></span></label>
                                  <input id="searchInput2" class="form-control" type="text" placeholder="Select Event Location">
                                  
                                  <div id="editMap"></div>
                               </article>
                            </div>

                           
                        </div>




                        <!-- /.box-body -->

                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>

                      <script  type="text/javascript" src="custom-admin-javascript/event/event_edit.js"></script>

                      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9x8mCn5-P8XUl59uGqwmmcU6Alt1qza8&libraries=places&callback=editMap" async defer></script>