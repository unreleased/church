<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
      media="all">

<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link rel="stylesheet" href="<?= $this->config->item('public_asset_path')?>css/animate.css">
<link rel="stylesheet" href="<?= $this->config->item('public_asset_path')?>css/lightbox.min.css">
<link rel="stylesheet" href="<?= $this->config->item('public_asset_path')?>css/bootstrap-touch-slider.css">
<link rel="stylesheet" href="<?= $this->config->item('public_asset_path')?>/css/grid-gallery.min.css">
<link rel="stylesheet" href="<?= $this->config->item('public_asset_path')?>css/style.css">


