<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">

        <a class="navbar-brand" href="index.html">
            <img class="navbar_logo" src="<?= $this->settings->site_logo() ?>">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse " id="navbarSupportedContent">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Pages
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="about-us">Abou Us</a>
                        <a class="dropdown-item" href="contact-us.html">Contact Us</a>
                        <a class="dropdown-item" href="#">Faq</a>
                        <a class="dropdown-item" href="#">Privacy Policy</a>
                        <a class="dropdown-item" href="#">Support</a>
                        <a class="dropdown-item" href="#">Terms of Use</a>
                        <a class="dropdown-item" href="<?= base_url()?>/gallery">Gallery</a>
                        <a class="dropdown-item" href="<?= base_url()?>/blogs">Blog</a>
                        <a class="dropdown-item" href="<?= base_url()?>staffs">Staff</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Sermons</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url()?>events">Event</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url()?>gallery">Gallery</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url()?>contact-us">Contact Us</a>
                </li>

            </ul>
            <div class=" ml-auto ss_extra_m"><a class="nav-link" href="#"> Donate Now </a></div>
        </div>
    </div>
</nav>