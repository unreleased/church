<?php

class Testimonial_model extends CI_Model
{

    public $table;

    public function __construct()
    {
        parent::__construct();
        $this->table = 'testimonial';
    }

    //---------------------------------------------------------Front----------------------------------------------------
    public function getTestimonials($limit)
    {
        $testimonials = $this->db->select('*')->from($this->table)->limit($limit)->get()->result_array();

        if (!empty($testimonials)) {
            $testimonials = array_map("self::put_related_items_in_testimonial", $testimonials);
        }

        return $testimonials;

    }

    public function put_related_items_in_testimonial($testimonial)
    {
        if (!empty($testimonial)) {
            $testimonial['testimonial_user_image_with_path'] = $this->config->item("default_user_image");
            if (!empty($testimonial['testimonial_user_image'])) {
                $testimonial['testimonial_user_image_with_path'] =
                    $this->config->item("testimonial_user_source_path") . $testimonial['testimonial_user_image'];
            }
        }

        return $testimonial;
    }

    //---------------------------------------------------------Backend--------------------------------------------------

    public function insert($table_name, $data)
    {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

    public function getAllData($tableName)
    {
        $this->db->select('*');
        $query = $this->db->get($tableName)->result_array();
        return $query;
    }

    public function deleteTestimonial($id)
    {
        $this->db->where('testimonial_key', $id);
        return $query = $this->db->delete('testimonial');
    }

    public function getTestimonialInfo($id)
    {
        $this->db->select('*');
        $this->db->from('testimonial');
        $this->db->where('testimonial_key', $id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function updateTestimonial($data)
    {
        $testimonial_key = $data['testimonial_key'];
        $this->db->set('testimonial_user_name', $data['testimonial_user_name']);
        $this->db->set('testimonial_description', $data['testimonial_description']);
        if (isset($data['testimonial_user_image'])) {
            $this->db->set('testimonial_user_image', $data['testimonial_user_image']);
        }
        $this->db->set('testimonial_updated_at', $data['testimonial_updated_at']);
        $this->db->where('testimonial_key', $testimonial_key);
        return $query = $this->db->update('testimonial');
    }

    public function deleteMassTestimonial($all_id)
    {
        $this->db->where_in('testimonial_key', $all_id);
        return $query = $this->db->delete('testimonial');
    }


     //Code for DataTable
    function get_datatables($table)
    {
        $this->_get_datatables_query($table);
        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result_array();
    }

    private function _get_datatables_query($table)
    {
        if($table=='testimonial'){
            $table = 'testimonial';
            $column_order = array('testimonial_user_name','testimonial_description',null); 
            $column_search = array('testimonial_user_name','testimonial_description'); 
            $order = array('testimonial_id' => 'desc');
        }
        $this->db->from($table);
        $i = 0;
        foreach ($column_search as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($order))
        {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function count_all($table)
    {
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    function count_filtered($table)
    {
        $this->_get_datatables_query($table);
        $query = $this->db->get();
        return $query->num_rows();
    }

}