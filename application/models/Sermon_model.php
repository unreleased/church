<?php
class Sermon_model extends CI_Model {

	public function insert($table_name,$data)
    {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

	public function getAllData($tableName)
    {
        $this->db->select('*');
        $query = $this->db->get($tableName)->result_array();
        return $query;
    }

    public function getAllPastor(){
        $this->db->select('staff_id, staff_name');
        $this->db->from('staff'); 
        $this->db->join('staff_type', 'staff_type.staff_type_id=staff.staff_type_id', 'left');
        $this->db->where('staff_type.staff_type_shortname', 'pastor');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getSermonInfo($sermon_key){
        $this->db->select('*');
        $this->db->from('sermon'); 
        $this->db->where('sermon_key', $sermon_key);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getSermonFile($sermon_id, $sermon_file_type){
        $this->db->select('*');
        $this->db->from('sermon_file'); 
        $this->db->where('sermon_id', $sermon_id);
        $this->db->where('sermon_file_type', $sermon_file_type);
        $result = $this->db->get();
        return $result->result_array();
    }


    public function updateSermon($data,$sermon_id){
        $this->db->where('sermon_id', $sermon_id);
        if($this->db->update('sermon', $data)){
            return TRUE;
        }
        else {return FALSE;}
    }


    public function check_sermon_file($sermon_id, $type){
        $this->db->select('*');
        $this->db->from('sermon_file'); 
        $this->db->where('sermon_id', $sermon_id);
        $this->db->where('sermon_file_type', $type);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function update_sermon_files($sermon_id, $sermon_file_link_type, $sermon_file, $type)
    {   
        $this->db->set('sermon_file_link_type', $sermon_file_link_type);
        $this->db->set('sermon_file', $sermon_file);
        $this->db->set('sermon_file_updated_at', date('Y-m-d H:i:s'));
        $this->db->where('sermon_id', $sermon_id);
        $this->db->where('sermon_file_type', $type);
        return  $query=$this->db->update('sermon_file');
    }

    public function getSermonTag($sermon_id){
        $this->db->select('*');
        $this->db->from('sermon_tag'); 
        $this->db->where('sermon_id', $sermon_id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function deletePreviousTag($sermon_id)
    {
        $this->db->where('sermon_id', $sermon_id);
        return $query = $this->db->delete('sermon_tag');
    }

    public function deleteSermon($sermon_key)
    {
        $this->db->where('sermon_key', $sermon_key);
        return $query = $this->db->delete('sermon');
    }

    public function deleteSermonFile($sermon_id)
    {
        $this->db->where('sermon_id', $sermon_id);
        return $query = $this->db->delete('sermon_file');
    }
    public function deleteSermonTag($sermon_id)
    {
        $this->db->where('sermon_id', $sermon_id);
        return $query = $this->db->delete('sermon_tag');
    }

    

	
}