<?php
class Admin_helper_model extends CI_Model {
    
   //event available seat
    public function event_available_seat($event_id){
        $event_info = $this->get_eventInfo($event_id);
        if(empty($event_info)){ return; }
        $total_event_seat       = $event_info[0]['event_seat'];
        $total_seat_booking     = $this->total_seat_booking($event_id);
        $total_available_seat   = $total_event_seat - $total_seat_booking;
        return $total_available_seat;
    }

    public function get_eventInfo($event_id){
        $this->db->select('*');
        $this->db->from('event'); 
        $this->db->where('event_id', $event_id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function total_seat_booking($event_id){
        $this->db->select('sum(event_booking.event_booking_seat) as total_booking');
        $this->db->from('event_booking'); 
        $this->db->where('event_id', $event_id);
        $this->db->where('event_booking_status', 1);
        $result = $this->db->get();
        $ret = $result->result_array();
        return $ret[0]['total_booking'];
    }

    /* *********************************************************** */



        
	
}