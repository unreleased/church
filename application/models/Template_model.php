<?php

class Template_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

    }

    public function render($file, $data)
    {
        $view = $this->load->view($file, $data, true);

        return $view;
    }

    public function footer($footer_params)
    {
        $data = array();
        $data['site_logo'] = $this->settings->site_logo();

        $data['address'] = "";
        $data['email'] = "";
        $data['phone'] = "";

        $main_settings = $this->settings->prepared_main_settings();

        if (!empty($main_settings)) {
            $data['address'] = isset($main_settings['address']) ?  $main_settings['address']: "";
            $data['email'] = isset($main_settings['email']) ?  $main_settings['email']: "";
            $data['phone'] = isset($main_settings['phone']) ?  $main_settings['phone']: "";
        }

        $data['footer_text'] = $this->settings->footer_text();
        $data['social_settings'] = $this->settings->prepared_social_settings();
        return $this->render('segments/footer', $data, true);
    }

}