<?php
class AdminModel extends CI_Model {

	public function insert($table_name,$data)
    {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

	public function getAllData($tableName)
    {
        $this->db->select('*');
        $query = $this->db->get($tableName)->result_array();
        return $query;
    }

    public function get_subcategory($id){
        $this->db->select('*');
        $this->db->from('sub_category'); 
        $this->db->where('category_id', $id);
        $result = $this->db->get();
        return $result->result_array();
    }

     public function checkAdmin($email){
        $this->db->select('*');
        $this->db->from('admin'); 
        $this->db->where('email', $email);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getAllAdmin(){
        $this->db->select('*');
        $this->db->from('admin'); 
        $this->db->where('role!=', 0);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getAdminInfo($id){
        $this->db->select('*');
        $this->db->from('admin'); 
        $this->db->where('id', $id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function updateAdmin($data)
    {   
    	$id = $data['id'];       
        $this->db->set('name', $data['name']);
        $this->db->set('email', $data['email']);
        if(isset($data['password'])){
        	$this->db->set('password', $data['password']);
        }
        $this->db->where('id', $id);
        return  $query=$this->db->update('admin');
    }

    public function deleteAdmin($id)
    {
        $this->db->where('id', $id);
        return $query = $this->db->delete('admin'); 
    }

    
      public function getAdminPermissionInfo($admin_id){
            $this->db->select('admin_permission.*, sub_category_name, category_name');
            $this->db->from('admin_permission'); 
            $this->db->join('sub_category', 'sub_category.id=admin_permission.sub_category_id', 'left');
            $this->db->join('category', 'category.id=sub_category.category_id', 'left');
            $this->db->where('admin_permission.admin_id', $admin_id);
            $result = $this->db->get();
            return $result->result_array();
        }


    public function check_admin_permission($admin_id, $sub_category_id){
        $this->db->select('*');
        $this->db->from('admin_permission'); 
        $this->db->where('admin_id', $admin_id);
        $this->db->where('sub_category_id', $sub_category_id);
        $result = $this->db->get();
        return $result->result_array();
    }

     public function deleteAdminPermission($id)
    {
        $this->db->where('id', $id);
        return $query = $this->db->delete('admin_permission'); 
    }

    
    public function getChallengePermissionInfo($admin_id){                

          $this->db->where('admin_id', $admin_id);
          $challengePermission = $this->db->get('permission')->result_array();
          return $challengePermission[0];

    }

    public function changeChallengePermissionValue($admin_id,$permission)
    {       
        $this->db->set('permission', $permission);
        $this->db->where('admin_id', $admin_id);
        return  $query=$this->db->update('permission');
    }

    public function updateNotice($data)
    {   
        $id = $data['id'];       
        $this->db->set('notice', $data['notice']);
        $this->db->where('id', $id);
        return  $query=$this->db->update('notice');
    }

    public function deleteNotice($id)
    {
        $this->db->where('id', $id);
        return $query = $this->db->delete('notice'); 
    }

    ////////////////
     public function get_notice_for_notification(){
        $this->db->select('*');
        $this->db->from('notice'); 
        $this->db->where('send_notification!=', 'done');
        $result = $this->db->get();
        return $result->result_array();
    }

     public function get_user_for_notification($start_user_id){
        $this->db->select('*');
        $this->db->from('user'); 
        $this->db->where('id >', $start_user_id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_last_user(){
        $this->db->select('*');
        $this->db->from('user'); 
        $this->db->order_by("id", "DESC");
        $this->db->limit(1);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function update_send_notification($notice_id, $userId)
    { 
        $this->db->set('send_notification', $userId);
        $this->db->where('id', $notice_id);
        return  $query=$this->db->update('notice');
    }

    public function changeNotificationStatus($notice_id)
    { 
        $this->db->set('send_notification', 'done');
        $this->db->where('id', $notice_id);
        return  $query=$this->db->update('notice');
    }

	


	
}