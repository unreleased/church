<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminDashboardController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Dashboard_model');
        $loggedUserId = $this->session->userdata('id');
        if (!isset($loggedUserId)) {
            redirect('admin/login');
        }
    }

    
    
    public function index()
    {   
        $data['page_title']     = 'Dashboard';
        $data['active_link']    = 'dashboard';
        $data['headerlink']     = $this->load->view('admin_views/templates/headerlink', $data, true);
        $data['header']         = $this->load->view('admin_views/templates/header', '', true);
        $data['sidebar']        = $this->load->view('admin_views/templates/sidebar', $data, true);
        $data['total_user']     = count($this->Dashboard_model->getAllData('user'));
        $data['total_sermon']   = count($this->Dashboard_model->getAllData('sermon'));
        $data['total_blog']     = count($this->Dashboard_model->getAllData('blog'));
        $data['total_event']    = count($this->Dashboard_model->getAllData('event'));
        $data['total_booking']  = count($this->Dashboard_model->getBooking());
        $data['today_booking']  = count($this->Dashboard_model->todayBooking());
        // echo "<pre>"; print_r($data['total_booking']); die;

        $data['footer']         = $this->load->view('admin_views/templates/footer', '', true);
        $data['content']        = $this->load->view('admin_views/dashboard/dashboard', $data, true);
        $this->load->view('admin_views/index', $data);
    }
}
