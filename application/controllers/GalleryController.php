<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GalleryController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model("Gallery_model");
    }

    public function index()
    {
        $data = array();

        $data['meta'] = $this->template->render('segments/meta', array('title'=>"Gallery"), true);
        $data['headlink'] = $this->template->render('segments/headlink', array(), true);
        $data['navbar'] = $this->template->render('segments/navbar', array(), true);
        $data['footer'] = $this->template->footer(array());
        $data['footlink'] = $this->template->render('segments/footlink', array(), true);

        $content_params = array();
        $data['content'] = $this->gallery_content($content_params);

        $view = $this->load->view('master', $data, true);
        echo $view;
        exit;
    }


    public function gallery_content($content_params)
    {
        $data = array();

        $data['gallery_items'] = array();
        $data['gallery_categories'] = $this->Gallery_model->getGalleryCategories();

        $content = $this->template->render('contents/gallery/gallery_page', $data, true);

        return $content;
    }

    public function ajax_gallery_items()
    {
        $data = array();

        $category_key = $this->input->get("category_key");
        $page_input = $this->input->get("page");
        if (empty($page_input)) {
            $page_input = 1;
        }

        $limit = 3; //get from settings
        $page = $page_input;

        $starts = ($page - 1) * $limit;

        $data['gallery_items'] = $this->Gallery_model->getGalleryItems($category_key,$limit, $starts);

        if (empty($data['gallery_items'])) {
            echo " ";exit;
        }

        $content = $this->template->render('contents/gallery/gallery_items', $data, true);

        echo $content;
        exit;
    }


}