<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminStaffController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Staff_model');
        $this->load->model('Important_model');
        $loggedUserId = $this->session->userdata('id');
        if (!isset($loggedUserId)) {
            redirect('admin/login');
        }
    }



    //**********************
    // view Staff
    // *********************
    public function viewStaff()
    {
        $permission = $this->permission->hasAccess(array('view_staff'));
        if($permission['view_staff']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        }  

        if ($this->input->server('REQUEST_METHOD') != 'POST') {

            $data['page_title']         = 'Staff';
            $data['active_link']        = 'staff';
            $data['headerlink']         = $this->load->view('admin_views/templates/headerlink', $data, true);
            $data['header']             = $this->load->view('admin_views/templates/header', '', true);
            $data['sidebar']            = $this->load->view('admin_views/templates/sidebar', '', true);                         
            $view_staff         = $this->Staff_model->getAllStaff();  
            $i=0;
            foreach ($view_staff as $staff) {                
                $staff_id = $staff['staff_id'];                
                $feature_image = $this->Staff_model->viewFeatureImage($staff_id);
                $view_staff[$i++]['feature_image'] = $feature_image;
            }

            $data['view_staff'] = $view_staff;    
            $data['view_staff_type']          = $this->Staff_model->getAllStaffType();    
            $data['content']            = $this->load->view('admin_views/staff/staff', $data, true);
            $dataJS['custom_js']        = array('staff/staff');
            $data['footer']             = $this->load->view('admin_views/templates/footer', $dataJS, true);
            $this->load->view('admin_views/index', $data);
        } else {
            $permission = $this->permission->hasAccess(array('add_staff'));
            if($permission['add_staff']==0){
                $this->session->set_flashdata('error_msg', 'Access Denied');
                redirect('admin/dashboard');
            }  

            $this->form_validation->set_rules('staff_name', 'Staff Name', 'required');   
            $this->form_validation->set_rules('staff_type_id', 'Staff Type', 'required'); 
            $this->form_validation->set_rules('staff_order', 'Staff Order', 'required'); 
            $this->form_validation->set_rules('email', 'Email', 'required'); 
            $this->form_validation->set_rules('password', 'Password', 'required'); 

            if($this->form_validation->run() == FALSE) {
              $this->session->set_flashdata('error_msg', 'Fill all required fields');
              redirect('admin/staff');
            }

            $post  = $this->input->post();
            $clean = $this->security->xss_clean($post);
            $key    = $this->Important_model->generate_key('staff', 'staff_key', 'stf');
            $image  = $this->input->post('image');            
            $data['staff_name']                 = $clean['staff_name'];
            $data['staff_type_id']              = $clean['staff_type_id'];
            $data['staff_description']          = $clean['staff_description'];
            $data['staff_order']                = $clean['staff_order'];
            $data['email']                      = $clean['email'];
            $data['password']                   = md5($clean['password']);
            $data['staff_key']                  = $key;            
            $data['is_staff_active']            = 1;
            $data['staff_created_at']           = date('Y-m-d H:i:s');

            $checkResult = $this->Staff_model->checkStaffEmail($data['email']);
            if (count($checkResult) > 0) {
                $this->session->set_flashdata('error_msg', 'This Email already exist');
                redirect('admin/staff');
            }

            $result = $this->Staff_model->insert('staff', $data);
            
            //code for four social media link upload
            if($result){ 
                for($i=0; $i<4; $i++){
                    if($i==0){
                        $data2['staff_social_link_name']        = 'facebook'; 
                        $data2['staff_social_link_url']         = $clean['facebook']; 
                    }
                    if($i==1){
                        $data2['staff_social_link_name']        = 'twitter'; 
                        $data2['staff_social_link_url']         = $clean['twitter']; 
                    }
                    if($i==2){
                        $data2['staff_social_link_name']        = 'google'; 
                        $data2['staff_social_link_url']         = $clean['google']; 
                    }
                    if($i==3){
                        $data2['staff_social_link_name']        = 'youtube'; 
                        $data2['staff_social_link_url']         = $clean['youtube']; 
                    }
                    $data2['staff_id']                      = $result; 
                    $data2['staff_social_link_created_at']  = date('Y-m-d H:i:s');
                    $result2 = $this->Staff_model->insert('staff_social_link', $data2);

                }

            }
            
            ////////////////////////
            
             if(!empty($image)){
                for($j=0;$j<count($image);$j++){
                     $data3['staff_image_name']=$image[$j];
                    if($j==0){
                        $data3['is_staff_image_featured']=1;
                    }
                    else{
                        $data3['is_staff_image_featured']=0;
                    }
                    $data3['staff_id']                  = $result; 
                    $data3['staff_image_created_at']    = date('Y-m-d H:i:s');
                    $result3 = $this->Staff_model->insert('staff_image', $data3);
                }
             }

            if ($result) {
                $this->session->set_flashdata('success_msg', 'Added Successfully');
                redirect('admin/staff');
            }
        }
    }



    //*******************************
    // render Staff for edit
    // ******************************
    public function renderEditStaff()
    {
        $staff_id = $this->input->post('id');
        $data['staff_info']   =   $this->Staff_model->get_staff_info($staff_id);
        $data['view_staff_type']          = $this->Staff_model->getAllStaffType();      
        $id = $data['staff_info'][0]['staff_id'];
        $data['facebook_url']   = $this->Staff_model->getStaffSocialLink($id,'facebook');
        $data['twitter_url']    = $this->Staff_model->getStaffSocialLink($id,'twitter');
        $data['google_url']     = $this->Staff_model->getStaffSocialLink($id,'google');
        $data['youtube_url']    = $this->Staff_model->getStaffSocialLink($id,'youtube');

        $data['view_staff_image']    = $this->Staff_model->getStaffImage($id);
        $json = array();
        $json['staff_info']            = $data['staff_info'];
        $json['view_staff_type']       = $data['view_staff_type'];
        $json['facebook_url']          = $data['facebook_url'];
        $json['twitter_url']           = $data['twitter_url'];
        $json['google_url']            = $data['google_url'];
        $json['youtube_url']           = $data['youtube_url'];
        $json['edit_staff_div'] = $this->load->view('admin_views/staff/edit_staff_div', $data, TRUE);
        echo json_encode($json);
    }


     //**********************
    // update Staff
    // *********************
    public function updateStaff()
    { 
        $permission = $this->permission->hasAccess(array('edit_staff'));
        if($permission['edit_staff']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        }     
        
        $this->form_validation->set_rules('staff_name', 'Staff Name', 'required');   
        $this->form_validation->set_rules('staff_type_id', 'Staff Type', 'required'); 
        $this->form_validation->set_rules('staff_order', 'Staff Order', 'required'); 

        if($this->form_validation->run() == FALSE) {
          $this->session->set_flashdata('error_msg', 'Fill all required fields');
          redirect('admin/staff');
        }

        else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post); 

             ////////////////////////
             
            $image  = $clean['image2'];            
             if(!empty($image)){
                $result2 = $this->Staff_model->deletePrevStaffImage($clean['staff_id']);
                for($j=0;$j<count($image);$j++){
                     $data3['staff_image_name']=$image[$j];
                    if($clean['staff_feature_image']==$data3['staff_image_name']){
                        $data3['is_staff_image_featured']=1;
                    }
                    else{
                        $data3['is_staff_image_featured']=0;
                    }
                    $data3['staff_id']                  = $clean['staff_id']; 
                    $data3['staff_image_created_at']    = date('Y-m-d H:i:s');
                    $result3 = $this->Staff_model->insert('staff_image', $data3);
                }
             }          

            $data['staff_name']                  = $clean['staff_name'];
            $data['staff_type_id']               = $clean['staff_type_id'];
            $data['staff_description']           = $clean['staff_description'];
            $data['staff_order']                 = $clean['staff_order'];
            $data['staff_id']                    = $clean['staff_id'];
            $data['is_staff_active']             = $clean['is_staff_active'];
            if(!empty($clean['password'])){
                $data['password']                = $clean['password'];
            }

            $data['staff_updated_at']            = date('Y-m-d H:i:s');
            $result = $this->Staff_model->updateStaff($data);            
            if ($result) {           
              
              $deleteResult = $this->Staff_model->deletePrevSocialLink($clean['staff_id']); 
               for($i=0; $i<4; $i++){
                    if($i==0){
                        $data2['staff_social_link_name']        = 'facebook'; 
                        $data2['staff_social_link_url']         = $clean['facebook']; 
                    }
                    if($i==1){
                        $data2['staff_social_link_name']        = 'twitter'; 
                        $data2['staff_social_link_url']         = $clean['twitter']; 
                    }
                    if($i==2){
                        $data2['staff_social_link_name']        = 'google'; 
                        $data2['staff_social_link_url']         = $clean['google']; 
                    }
                    if($i==3){
                        $data2['staff_social_link_name']        = 'youtube'; 
                        $data2['staff_social_link_url']         = $clean['youtube']; 
                    }
                    $data2['staff_id']                      = $clean['staff_id']; 
                    $data2['staff_social_link_created_at']  = date('Y-m-d H:i:s');
                    $result2 = $this->Staff_model->insert('staff_social_link', $data2);

                }

              
                $this->session->set_flashdata('success_msg', 'Updated Successfully');
                redirect('admin/staff');
            }

        }
    }


    //**********************
    // delete Staff Image
    // *********************
    public function deleteStaffImage()
    {
        $id = $this->input->post('staff_image_id'); 
        $result = $this->Staff_model->deleteStaffImage($id);
        if (count($result) > 0) {                    
            echo "Deleted Successfully";
        }
    }




//**************************
// image upload by dropzone
// *************************
public function saveImage() {
       $files = $_FILES;
       if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
           $config['encrypt_name'] = TRUE;
           $config['upload_path'] = $this->config->item('staff_upload_path');
           $config['allowed_types'] = 'jpg|png|jpeg';
           $config['max_size'] = '0';
           $this->load->library('upload', $config);
           $this->upload->initialize($config);
           if (!$this->upload->do_upload('userfile')) {
               $status = 'error';
               $msg = $this->upload->display_errors('', '');
           } else {
               $fileinfo = $this->upload->data();
               echo $fileinfo['file_name'];
           }
       }
   }



    //**********************
    // delete Staff All Info
    // *********************
    public function deleteStaff()
    {
        $permission = $this->permission->hasAccess(array('delete_staff'));
        if($permission['delete_staff']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        }  

        $staff_key = $this->input->post('staff_key');
        $get_staff_info = $this->Staff_model->getStaffInfo($staff_key);
       
        $staff_id = $get_staff_info[0]['staff_id'];
        $get_staff_image = $this->Staff_model->getStaffImage($staff_id);

        if(!empty($get_staff_image)){
           for($i=0; $i<count($get_staff_image); $i++){
            $image_name = $get_staff_image[$i]['staff_image_name'];  
            $staff_image_id = $get_staff_image[$i]['staff_image_id'];  

            $path = $this->config->item('staff_upload_path').$image_name;
                if(!empty($image_name) && file_exists($path)){
                    unlink($path);
                }
            $result = $this->Staff_model->deleteStaffImage($staff_image_id); 
           } 
        } 

        $result2 = $this->Staff_model->deleteStaffSocialLink($staff_id);
        $result3 = $this->Staff_model->deleteStaff($staff_key);

         if (count($result3) > 0) {                    
            echo "Deleted Successfully";
        }
        
    }


    //**********************
    // Mass Type Delete
    // *********************
    public function massStaffTypeDelete()
    {
        $ids = $this->input->post('ids'); 
        $all_id = explode(",",$ids);
        $result = $this->Staff_model->deleteMassStaffType($all_id);
        if($result){
            $this->session->set_flashdata('success_msg', 'Deleted Successfully');
            echo "Delete Successfully";
        }
    }



    //**********************
    // View Staff Type
    // *********************
    public function staffType()
    {
        $permission = $this->permission->hasAccess(array('view_staff_type'));
        if($permission['view_staff_type']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        } 

        if ($this->input->server('REQUEST_METHOD') != 'POST') {
            $data['page_title']     = 'Staff Type';
            $data['active_link']    = 'staff_type';
            $data['headerlink']     = $this->load->view('admin_views/templates/headerlink', $data, true);
            $data['header']         = $this->load->view('admin_views/templates/header', '', true);
            $data['sidebar']        = $this->load->view('admin_views/templates/sidebar', '', true); 
            //$data['view_staff_type']= $this->Staff_model->getAllStaffType();            
            $data['content']        = $this->load->view('admin_views/staff/staff_type', $data, true);
            $dataJS['custom_js']        = array('staff/add_staff_type');
            $data['footer']         = $this->load->view('admin_views/templates/footer', $dataJS, true);
            $this->load->view('admin_views/index', $data);
        }
        else {

            $permission = $this->permission->hasAccess(array('add_staff_type'));
            if($permission['add_staff_type']==0){
                $this->session->set_flashdata('error_msg', 'Access Denied');
                redirect('admin/dashboard');
            } 

            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);
            $this->form_validation->set_rules('staff_type_name', 'Staff Type Name', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_msg', 'Validation Error');
                redirect('admin/add-staff-type');
            }
            $key = $this->Important_model->generate_key('staff_type', 'staff_type_key', 'staff_type');
            
            $permission = array('add_user','view_user','edit_user','delete_user','add_gallery','view_gallery','edit_gallery','delete_gallery','add_gallery_image','view_gallery_image','edit_gallery_image','delete_gallery_image','add_sermon','view_sermon','edit_sermon','delete_sermon','add_banner','view_banner','edit_banner','delete_banner','add_blog','view_blog','edit_blog','delete_blog','add_staff_type','view_staff_type','edit_staff_type','delete_staff_type','add_staff','view_staff','edit_staff','delete_staff','add_testimonial','view_testimonial','edit_testimonial','delete_testimonial','add_event','view_event','edit_event','delete_event','add_booking','view_booking','edit_booking','delete_booking');

            $staff_type_name = $clean['staff_type_name'];
            $temp = $clean;
            unset($temp['staff_type_name']);
            foreach($permission as $access){
                if( $this->input->post($access) ){
                    $temp[$access] = 1;
                }else{
                    $temp[$access] = 0;
                }
            }
            unset($clean);
            $data['staff_type_key']           = $key;
            $data['staff_type_name']          = $staff_type_name;
            $data['staff_type_permission']    = json_encode($temp);
            $data['staff_type_shortname']     = strtolower(str_replace(' ', '', $staff_type_name));
            $data['is_staff_type_deletable']  = 1;
            
            
            $data['staff_type_created_at']     = date('Y-m-d H:i:s');
            $checkResult = $this->Staff_model->checkStaffType($staff_type_name);
            if (count($checkResult) > 0) {
                $this->session->set_flashdata('error_msg', 'This Name already exist');
                redirect('admin/add-staff-type');
            }
            $result = $this->Staff_model->insert('staff_type', $data);
            if ($result) {
                $this->session->set_flashdata('success_msg', 'Added Successfully');
                redirect('admin/staff-type');
            }
        }
       
    }

    public function addStaffType()
    {
        $permission = $this->permission->hasAccess(array('add_staff_type'));
        if($permission['add_staff_type']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        } 

        $data['page_title']     = 'Add Staff Type';
        $data['active_link']    = 'staff_type';
        $data['headerlink']     = $this->load->view('admin_views/templates/headerlink', $data, true);
        $data['header']         = $this->load->view('admin_views/templates/header', '', true);
        $data['sidebar']        = $this->load->view('admin_views/templates/sidebar', '', true);
        $data['content']        = $this->load->view('admin_views/staff/add_staff_type', $data, true);
        $dataJS['custom_js']  = array('staff/add_staff_type');
        $data['footer']         = $this->load->view('admin_views/templates/footer', $dataJS, true);
        $this->load->view('admin_views/index', $data);

    }

    public function editStaffType($staff_type_key)
    {
        $permission = $this->permission->hasAccess(array('edit_staff_type'));
        if($permission['edit_staff_type']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        }

        $data['page_title']     = 'Edit Staff Type';
        $data['active_link']    = 'staff_type';
        $data['headerlink']     = $this->load->view('admin_views/templates/headerlink', $data, true);
        $data['header']         = $this->load->view('admin_views/templates/header', '', true);
        $data['sidebar']        = $this->load->view('admin_views/templates/sidebar', '', true); 
        $data['staff_type_info']= $this->Staff_model->getStaffTypeInfo($staff_type_key);
        // print_r($data['staff_type_info']); die;
        if(empty($data['staff_type_info'])){
            $this->session->set_flashdata('error_msg', 'Something May Wrong!');
            redirect('admin/staff-type');        }
        $data['permission']     = (array)json_decode($data['staff_type_info'][0]['staff_type_permission']);
        $data['content']        = $this->load->view('admin_views/staff/edit_staff_type', $data, true);
        $dataJS['custom_js']    = array('staff/add_staff_type');
        $data['footer']         = $this->load->view('admin_views/templates/footer', $dataJS, true);
        $this->load->view('admin_views/index', $data);

    }

    public function updateStaffType(){
        $permission = $this->permission->hasAccess(array('edit_staff_type'));
        if($permission['edit_staff_type']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        }

        $this->form_validation->set_rules('staff_type_name', 'Staff Type Name', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_msg', 'Please Fill up the required field');
            redirect('admin/staff-type');
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);   
            $staff_type_key  = $clean['staff_type_key'];
            $staff_type_name = $clean['staff_type_name'];

            $staff_type_info = $this->Staff_model->getStaffTypeInfo($staff_type_key);
            $prev_staff_type_name = $staff_type_info[0]['staff_type_name'];

            if($prev_staff_type_name != $clean['staff_type_name']){
                $checkStaffTypeName = $this->Staff_model->checkStaffType($clean['staff_type_name']);
                if($checkStaffTypeName){
                    $this->session->set_flashdata('error_msg', 'This name already exist!');
                    redirect('admin/staff-type');
                }
            }

          $permission = array('add_user','view_user','edit_user','delete_user','add_gallery','view_gallery','edit_gallery','delete_gallery','add_gallery_image','view_gallery_image','edit_gallery_image','delete_gallery_image','add_sermon','view_sermon','edit_sermon','delete_sermon','add_banner','view_banner','edit_banner','delete_banner','add_blog','view_blog','edit_blog','delete_blog','add_staff_type','view_staff_type','edit_staff_type','delete_staff_type','add_staff','view_staff','edit_staff','delete_staff','add_testimonial','view_testimonial','edit_testimonial','delete_testimonial','add_event','view_event','edit_event','delete_event','add_booking','view_booking','edit_booking','delete_booking');

            $temp = $clean;
            unset($temp['staff_type_name'], $temp['staff_type_key']);

            foreach($permission as $access){
                if( $this->input->post($access) ){
                    $temp[$access] = 1;
                }else{
                    $temp[$access] = 0;
                }
            }
            unset($clean);
            $clean['staff_type_permission']   = json_encode($temp);
            $clean['staff_type_name']         = $staff_type_name; 
            $clean['staff_type_shortname']    = strtolower(str_replace(' ', '', $staff_type_name));
            $clean['staff_type_updated_at']   = date('Y-m-d H:i:s');            
            $clean['staff_type_key']          = $staff_type_key; 

            $result = $this->Staff_model->updateStaffType($clean);
            if ($result) {
                $this->session->set_flashdata('success_msg', 'Updated Succesfully');
                redirect('admin/staff-type');
            }
        }
    }

    public function deleteStaffType()
    {
        $permission = $this->permission->hasAccess(array('delete_staff_type'));
        if($permission['delete_staff_type']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        }

        $staff_type_key = $this->input->post('staff_type_key');
        $result = $this->Staff_model->deleteStaffType($staff_type_key);
        if (count($result) > 0) {
            echo "Deleted Successfully";
        }
    }


    public function profile(){
        $staff_key = $this->session->userdata('staff_key');
        $staff_id  = $this->session->userdata('id');

        if ($this->input->server('REQUEST_METHOD') != 'POST') {
            $data['page_title']     = 'Profile';
            $data['active_link']    = 'profile';
            $data['headerlink']     = $this->load->view('admin_views/templates/headerlink', $data, true);
            $data['header']         = $this->load->view('admin_views/templates/header', '', true);
            $data['sidebar']        = $this->load->view('admin_views/templates/sidebar', '', true); 
            $data['staff_info']     = $this->Staff_model->getStaffInfo($staff_key);            
            $data['facebook_url']   = $this->Staff_model->getStaffSocialLink($staff_id,'facebook');
            $data['twitter_url']    = $this->Staff_model->getStaffSocialLink($staff_id,'twitter');
            $data['google_url']     = $this->Staff_model->getStaffSocialLink($staff_id,'google');
            $data['youtube_url']    = $this->Staff_model->getStaffSocialLink($staff_id,'youtube');
            $data['view_staff_image']    = $this->Staff_model->getStaffImage($staff_id);     
            $data['content']        = $this->load->view('admin_views/staff/profile', $data, true);
            $dataJS['custom_js']    = array('staff/profile');
            $data['footer']         = $this->load->view('admin_views/templates/footer', $dataJS, true);
            $this->load->view('admin_views/index', $data);
        } else {            
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);
            $image  = $clean['image2'];            
             if(!empty($image)){
                $result2 = $this->Staff_model->deletePrevStaffImage($clean['staff_id']);
                for($j=0;$j<count($image);$j++){
                     $data3['staff_image_name']=$image[$j];
                    if($clean['staff_feature_image']==$data3['staff_image_name']){
                        $data3['is_staff_image_featured']=1;
                    }
                    else{
                        $data3['is_staff_image_featured']=0;
                    }
                    $data3['staff_id']                  = $clean['staff_id']; 
                    $data3['staff_image_created_at']    = date('Y-m-d H:i:s');
                    $result3 = $this->Staff_model->insert('staff_image', $data3);

                }
             }
            $data['staff_name']                  = $clean['staff_name'];
            $data['staff_id']                    = $clean['staff_id'];
            $data['staff_description']           = $clean['staff_description'];            
            if(!empty($clean['password'])){
                $data['password']                = $clean['password'];
            }
            $data['staff_updated_at']            = date('Y-m-d H:i:s');
            $result = $this->Staff_model->updateProfile($data);  

            if ($result) {           
              
              $deleteResult = $this->Staff_model->deletePrevSocialLink($clean['staff_id']); 
               for($i=0; $i<4; $i++){
                    if($i==0){
                        $data2['staff_social_link_name']        = 'facebook'; 
                        $data2['staff_social_link_url']         = $clean['facebook']; 
                    }
                    if($i==1){
                        $data2['staff_social_link_name']        = 'twitter'; 
                        $data2['staff_social_link_url']         = $clean['twitter']; 
                    }
                    if($i==2){
                        $data2['staff_social_link_name']        = 'google'; 
                        $data2['staff_social_link_url']         = $clean['google']; 
                    }
                    if($i==3){
                        $data2['staff_social_link_name']        = 'youtube'; 
                        $data2['staff_social_link_url']         = $clean['youtube']; 
                    }
                    $data2['staff_id']                      = $clean['staff_id']; 
                    $data2['staff_social_link_created_at']  = date('Y-m-d H:i:s');
                    $result2 = $this->Staff_model->insert('staff_social_link', $data2);

                }              
                $this->session->set_flashdata('success_msg', 'Updated Successfully');
                redirect('admin/profile');
            }             
        }
    }



    //*************************************
    // load Staff Type table by ajax
    //************************************
    public function loadStaffTypeTable()
    {
        $list = $this->Staff_model->get_datatables('staff_type');
        $data = array();
        $no = $_POST['start'];
        $permission = $this->permission->hasAccess(array('edit_staff_type','delete_staff_type'));
        foreach ($list as $staff_type) {
            $no++;          
            $row = array();            
            $row[] = $no;
            $row[] = $staff_type['staff_type_name'];
            
            $role['action']="";
            if($permission['edit_staff_type']==1){
            $role['action'].="<a href='admin/edit-staff-type/{$staff_type["staff_type_key"]}'><i style='margin-right:10px; font-size: 16px;'' data-toggle='tooltip' title='Edit' class='fa fa-pencil'></i></a>";
            }

            if($permission['delete_staff_type']==1){
            $role['action'].="<a href='javascript:void(0)'><i data-delete_staff_type='{$staff_type["staff_type_key"]}' style='font-size: 16px' data-toggle='tooltip' title='Delete' class='fa fa-trash btn_delete_staff_type'></i></a>"; 
            }
            $row[] = $role['action'];
            
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Staff_model->count_all('staff_type'),
            "recordsFiltered" => $this->Staff_model->count_filtered('staff_type'),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
        die();
    }


    //*************************************
    // load Staff Table by ajax
    //************************************
    public function loadStaffTable()
    {
        $list = $this->Staff_model->get_datatables('staff');       
        $data = array();
        $no = $_POST['start'];
        $permission = $this->permission->hasAccess(array('edit_staff','delete_staff'));
        foreach ($list as $staff) {
            $no++;          
            $row = array();            
            $row[] = $no;
            $staff_id = $staff['staff_id'];
            $feature_image = $this->Staff_model->viewFeatureImage($staff_id);
            if(!empty($feature_image)){
                $image_name = $feature_image[0]['staff_image_name'];
            }
            if(isset($image_name)){
                $row[] = "<img src=".$this->config->item('staff_source_path').$image_name." style='height: 40px; width: 40px'>";
            }
            if(!isset($image_name)){ $row[] = "";}
            $row[] = $staff['staff_name']; 

            $staff_type_id = $staff['staff_type_id'];
            $type = $this->Staff_model->getTypeName($staff_type_id);
            if(!empty($type)){ $row[] = $type[0]['staff_type_name']; }
            if(empty($type)){ $row[] = ''; }
           
            $role['action']="";
            if($permission['edit_staff']==1){
            $role['action']="<a staff-key='{$staff["staff_key"]}' data-toggle='modal' class='edit_staff' data-target='#edit_staff' href='javascript:void(0)'><i style='margin-right:10px; font-size: 16px;'' data-toggle='tooltip' title='Edit' class='fa fa-pencil'></i></a>";
            }
            
            if($permission['delete_staff']==1){
            $role['action'].="<a href='javascript:void(0)'><i data-delete_staff='{$staff["staff_key"]}' style='font-size: 16px' data-toggle='tooltip' title='Delete' class='fa fa-trash btn_delete_staff'></i></a>";
            }
            
            $row[] = $role['action'];            
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Staff_model->count_all('staff'),
            "recordsFiltered" => $this->Staff_model->count_filtered('staff'),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
        die();
    }



    



    

}
