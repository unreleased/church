<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminSermonController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Sermon_model');
        $this->load->model('Important_model');
        $loggedUserId = $this->session->userdata('id');
        if (!isset($loggedUserId)) {
            redirect('admin/login');
        }
        
    }

    public function viewSermon(){
        
        if ($this->input->server('REQUEST_METHOD') != 'POST') {

            $data['page_title']         = 'Sermons';
            $data['active_link']        = 'sermon';
            $data['headerlink']         = $this->load->view('admin_views/templates/headerlink', $data, true);
            $data['header']             = $this->load->view('admin_views/templates/header', '', true);
            $data['sidebar']            = $this->load->view('admin_views/templates/sidebar', '', true);            
            $data['all_sermon'] = $this->Sermon_model->getAllData('sermon'); 
            $data['all_pastor'] = $this->Sermon_model->getAllPastor();     
            $data['content']        = $this->load->view('admin_views/sermon/sermon', $data, true);
            $dataJS['custom_js']    = array('sermon/sermon');
            $data['footer']         = $this->load->view('admin_views/templates/footer', $dataJS, true);
            $this->load->view('admin_views/index', $data);
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);
            $this->form_validation->set_rules('sermon_title', 'Sermon Title', 'required');
            $this->form_validation->set_rules('staff_id', 'Staff ID', 'required');
            $this->form_validation->set_rules('sermon_description', 'Sermon Description', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_msg', 'Validation Error');
                redirect('admin/sermon');
            }
            $key = $this->Important_model->generate_key('sermon', 'sermon_key', 'sermon');
            $data['sermon_key']               = $key;
            $data['staff_id']                 = $clean['staff_id'];
            $data['sermon_title']             = $clean['sermon_title'];            
            $data['sermon_description']       = $clean['sermon_description'];
            $data['sermon_order']             = $clean['sermon_order'];            
            $data['is_sermon_featured']       = $clean['is_sermon_featured'];            
            $data['sermon_created_at']        = date('Y-m-d H:i:s');

            //feature image upload code start
            if($_FILES){ 
                // print_r($_FILES); die;
                $sermon_feature_image = $this->sermon_file_upload('sermon_feature_image');
                if(isset($sermon_feature_image['sermon_file'])){
                    $data['sermon_feature_image'] = $sermon_feature_image['sermon_file'];
                }
            }
            //feature image upload code end 

            $result = $this->Sermon_model->insert('sermon', $data);
            if ($result) {
               
                /* ***************/
                /* Sermon Video */
                /* *************/
                if(isset($clean['sermon_video_type'])){
                    $key2 = $this->Important_model->generate_key('sermon_file', 'sermon_file_key', 'sermon_file_key');
                    $data2['sermon_file_key'] = $key2;
                    $data2['sermon_id'] = $result;
                    $data2['sermon_file_link_type']     = $clean['sermon_video_type'];
                    $data2['sermon_file_type']          = 'video';
                    $data2['sermon_file_created_at']    = date('Y-m-d H:i:s');
                   
                    if($clean['sermon_video_type']=='global'){                        
                        $data2['sermon_file']               = $clean['sermon_video'];
                    }
                    if($clean['sermon_video_type']=='local'){
                       if(!empty($_FILES['sermon_video']['name'])){
                            $sermon_video_upload = $this->sermon_file_upload('sermon_video');
                            if(isset($sermon_video_upload['sermon_file'])){
                                $data2['sermon_file']=$sermon_video_upload['sermon_file'];
                            }
                        } 
                    }
                    $result2 = $this->Sermon_model->insert('sermon_file', $data2); 
                }


                /* ***************/
                /* Sermon Audio */
                /* *************/
                 if(isset($clean['sermon_audio_type'])){
                    $key3 = $this->Important_model->generate_key('sermon_file', 'sermon_file_key', 'sermon_file_key');
                    $data3['sermon_file_key'] = $key3;
                    $data3['sermon_id'] = $result;
                    $data3['sermon_file_link_type']     = $clean['sermon_audio_type'];
                    $data3['sermon_file_type']          = 'audio';
                    $data3['sermon_file_created_at']    = date('Y-m-d H:i:s');
                   
                    if($clean['sermon_audio_type']=='global'){                        
                        $data3['sermon_file']               = $clean['sermon_audio'];
                    }
                    if($clean['sermon_audio_type']=='local'){
                       if(!empty($_FILES['sermon_audio']['name'])){
                            $sermon_audio_upload = $this->sermon_file_upload('sermon_audio');
                            if(isset($sermon_audio_upload['sermon_file'])){
                                $data3['sermon_file']=$sermon_audio_upload['sermon_file'];
                            }
                        } 
                    }
                    $result3 = $this->Sermon_model->insert('sermon_file', $data3); 
                }


                /* ***************/
                /* Sermon File */
                /* *************/                

                 if(isset($_FILES['sermon_file']['name'])){
                    $key4 = $this->Important_model->generate_key('sermon_file', 'sermon_file_key', 'sermon_file_key');
                    $data4['sermon_file_key'] = $key4;
                    $data4['sermon_id'] = $result;
                    $data4['sermon_file_link_type']     = 'local';
                    $data4['sermon_file_type']          = 'file';
                    $data4['sermon_file_created_at']    = date('Y-m-d H:i:s');                   
                    
                   if(!empty($_FILES['sermon_file']['name'])){
                        $sermon_file_upload = $this->sermon_file_upload('sermon_file');
                        if(isset($sermon_file_upload['sermon_file'])){
                            $data4['sermon_file']=$sermon_file_upload['sermon_file'];
                        }
                    }                    
                    $result4 = $this->Sermon_model->insert('sermon_file', $data4); 
                }

                /* ***************/
                /* Sermon Tags */
                /* *************/  
                $tag = $clean['sermon_tag'];
                for($i=0; $i<count($tag); $i++){                    
                    $data5['sermon_id'] = $result;
                    $data5['sermon_tag'] = $tag[$i];
                    $result5 = $this->Sermon_model->insert('sermon_tag', $data5);
                }
                $this->session->set_flashdata('success_msg', 'Added Successfully');
                redirect('admin/sermon');
            }
        }
    }


    public function sermon_file_upload($file_field_name)
    {
        $config['encrypt_name']  = TRUE;
        $config['upload_path']   = $this->config->item('sermon_file_upload_path');
        if($file_field_name=='sermon_feature_image'){
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
        }
        if($file_field_name=='sermon_video'){
            $config['allowed_types'] = 'mp4|mpg|mpeg|avi';
        }
        if($file_field_name=='sermon_audio'){
            $config['allowed_types'] = 'mp3|ogg';
        }

        if($file_field_name=='sermon_file'){
            $config['allowed_types'] = 'doc|pdf';
        }

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload($file_field_name)) {
            $status = 'error';
            $data['msg'] = $this->upload->display_errors('', '');
        } else {
            $fileName=$this->upload->data();
            $data['sermon_file']=$fileName['file_name'];
        } 
        return $data;
    }





    //*******************************
    // render sermon for edit
    //******************************
    public function renderEditSermon()
    {
        $sermon_key = $this->input->post('sermon_key');
        $data['sermon_info']  = $this->Sermon_model->getSermonInfo($sermon_key); 
        $sermon_id            = $data['sermon_info'][0]['sermon_id'];
        $data['all_tag']      = $this->Sermon_model->getSermonTag($sermon_id); 
        $data['all_sermon']   = $this->Sermon_model->getAllData('sermon'); 
        $data['all_pastor']   = $this->Sermon_model->getAllPastor();
        $data['sermon_file']  = $this->Sermon_model->getSermonFile($sermon_id,'file');
        $data['sermon_video'] = $this->Sermon_model->getSermonFile($sermon_id,'video');
        $data['sermon_audio'] = $this->Sermon_model->getSermonFile($sermon_id,'audio');

        $json = array();
        $json['all_tag']         = $data['all_tag'];
        $json['sermon_info']     = $data['sermon_info'];
        $json['all_sermon']      = $data['all_sermon'];
        $json['all_pastor']      = $data['all_pastor'];
        $json['sermon_file']     = $data['sermon_file'];
        $json['sermon_video']    = $data['sermon_video'];
        $json['sermon_audio']    = $data['sermon_audio'];
        $json['edit_sermon_div'] = $this->load->view('admin_views/sermon/edit_sermon_div', $data, TRUE);
        echo json_encode($json);
    }


    public function updateSermon(){
        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);
        // echo "<pre>"; print_r($clean); die;            
        $this->form_validation->set_rules('sermon_title', 'Sermon Title', 'required');
        $this->form_validation->set_rules('staff_id', 'Staff ID', 'required');
        $this->form_validation->set_rules('sermon_description', 'Sermon Description', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_msg', 'Validation Error');
            redirect('admin/sermon');
        }        
        $sermon_id                        = $clean['sermon_id'];
        $data['staff_id']                 = $clean['staff_id'];
        $data['sermon_title']             = $clean['sermon_title'];            
        $data['sermon_description']       = $clean['sermon_description'];
        $data['sermon_order']             = $clean['sermon_order'];            
        $data['is_sermon_featured']       = $clean['is_sermon_featured'];            
        $data['sermon_updated_at']        = date('Y-m-d H:i:s');

        if(!empty($_FILES['sermon_feature_image']['name'])){
            $sermon_feature_image = $this->sermon_file_upload('sermon_feature_image');
            if(isset($sermon_feature_image['sermon_file'])){
                $data['sermon_feature_image'] = $sermon_feature_image['sermon_file'];
            }
        }
        $result = $this->Sermon_model->updateSermon($data, $sermon_id); 

        /* ***************/
        /* Sermon Video */
        /* *************/
        if(isset($clean['sermon_video_type'])){
            $sermon_video = $this->Sermon_model->check_sermon_file($sermon_id,'video');
            $prev_video_name = $sermon_video[0]['sermon_file']; 
            if(empty($sermon_video)){
                 $key2 = $this->Important_model->generate_key('sermon_file', 'sermon_file_key', 'sermon_file_key');
                    $data2['sermon_file_key'] = $key2;
                    $data2['sermon_id'] = $sermon_id;
                    $data2['sermon_file_link_type']     = $clean['sermon_video_type'];
                    $data2['sermon_file_type']          = 'video';
                    $data2['sermon_file_created_at']    = date('Y-m-d H:i:s');
                   
                    if($clean['sermon_video_type']=='global'){                        
                        $data2['sermon_file']               = $clean['sermon_video'];
                    }
                    if($clean['sermon_video_type']=='local'){
                       if(!empty($_FILES['sermon_video']['name'])){
                            $sermon_video_upload = $this->sermon_file_upload('sermon_video');
                            if(isset($sermon_video_upload['sermon_file'])){
                                $data2['sermon_file']=$sermon_video_upload['sermon_file'];
                            }
                        }
                    }
                    $result2 = $this->Sermon_model->insert('sermon_file', $data2); 
            }
            if(!empty($sermon_video)){
                if($clean['sermon_video_type']=='global'){                        
                        $data3['sermon_file']               = $clean['sermon_video'];
                    }
                    if($clean['sermon_video_type']=='local'){
                       if(!empty($_FILES['sermon_video']['name'])){
                            $sermon_video_upload = $this->sermon_file_upload('sermon_video');
                            if(isset($sermon_video_upload['sermon_file'])){
                                $data3['sermon_file']=$sermon_video_upload['sermon_file'];
                            }
                        } 

                        if(empty($_FILES['sermon_video']['name'])){
                            $data3['sermon_file'] = $prev_video_name;
                        }
                    }
                $video_update = $this->Sermon_model->update_sermon_files($sermon_id, $clean['sermon_video_type'], $data3['sermon_file'],'video'); 
            }
        }

        /* ***************/
        /* Sermon Audio */
        /* *************/
        if(isset($clean['sermon_audio_type'])){
            $sermon_audio = $this->Sermon_model->check_sermon_file($sermon_id,'audio');
            $prev_audio_name = $sermon_audio[0]['sermon_file']; 
            if(empty($sermon_audio)){
                 $key9 = $this->Important_model->generate_key('sermon_file', 'sermon_file_key', 'sermon_file_key');
                    $data9['sermon_file_key'] = $key9;
                    $data9['sermon_id'] = $sermon_id;
                    $data9['sermon_file_link_type']     = $clean['sermon_audio_type'];
                    $data9['sermon_file_type']          = 'audio';
                    $data9['sermon_file_created_at']    = date('Y-m-d H:i:s');
                   
                    if($clean['sermon_audio_type']=='global'){                        
                        $data9['sermon_file']               = $clean['sermon_audio'];
                    }
                    if($clean['sermon_audio_type']=='local'){
                       if(!empty($_FILES['sermon_audio']['name'])){
                            $sermon_audio_upload = $this->sermon_file_upload('sermon_audio');
                            if(isset($sermon_audio_upload['sermon_file'])){
                                $data9['sermon_file']=$sermon_audio_upload['sermon_file'];
                            }
                        } 
                    }
                    $result9 = $this->Sermon_model->insert('sermon_file', $data9); 
            }
            if(!empty($sermon_audio)){
                if($clean['sermon_audio_type']=='global'){                        
                        $data8['sermon_file']               = $clean['sermon_audio'];
                    }
                    if($clean['sermon_audio_type']=='local'){
                       if(!empty($_FILES['sermon_audio']['name'])){
                            $sermon_audio_upload = $this->sermon_file_upload('sermon_audio');
                            if(isset($sermon_audio_upload['sermon_file'])){
                                $data8['sermon_file']=$sermon_audio_upload['sermon_file'];
                            }
                        } 

                        if(empty($_FILES['sermon_audio']['name'])){
                            $data8['sermon_file'] = $prev_audio_name;
                        }
                    }
                $audio_update = $this->Sermon_model->update_sermon_files($sermon_id, $clean['sermon_audio_type'], $data8['sermon_file'],'audio'); 
            }
        }

        /* ***************/
        /* Sermon File */
        /* *************/        
        if(isset($_FILES['sermon_file']['name'])){           
            $key7 = $this->Important_model->generate_key('sermon_file', 'sermon_file_key', 'sermon_file_key');
            $data7['sermon_file_key'] = $key7;
            $data7['sermon_id'] = $sermon_id;
            $data7['sermon_file_link_type']     = 'local';
            $data7['sermon_file_type']          = 'file';
            $data7['sermon_file_created_at']    = date('Y-m-d H:i:s');                   
            
           if(!empty($_FILES['sermon_file']['name'])){
                $sermon_file_upload = $this->sermon_file_upload('sermon_file');
                if(isset($sermon_file_upload['sermon_file'])){
                    $data7['sermon_file']=$sermon_file_upload['sermon_file'];
                }
            }

            $sermon_file = $this->Sermon_model->check_sermon_file($sermon_id,'file');
            if(empty($sermon_file)){
                $result7 = $this->Sermon_model->insert('sermon_file', $data7); 
            }
            if(!empty($sermon_file)){
                $file_update = $this->Sermon_model->update_sermon_files($sermon_id, 'local', $data7['sermon_file'],'file'); 
            }
        }

        /* ***************/
        /* Sermon Tag   */
        /* *************/ 
        $deleteResult = $this->Sermon_model->deletePreviousTag($sermon_id);
        $tag = $clean['sermon_tag'];
        for($i=0; $i<count($tag); $i++){                    
            $data9['sermon_id'] = $sermon_id;
            $data9['sermon_tag'] = $tag[$i];
            $result9 = $this->Sermon_model->insert('sermon_tag', $data9);
        }

        if($result){
            $this->session->set_flashdata('success_msg', 'Updated Successfully');
            redirect('admin/sermon');
        }
    }

    public function deleteSermon(){
        $sermon_key     = $this->input->post('sermon_key'); 
        $sermon_info    = $this->Sermon_model->getSermonInfo($sermon_key);
        $sermon_id = $sermon_info[0]['sermon_id'];
        $result     = $this->Sermon_model->deleteSermon($sermon_key);
        $result2     = $this->Sermon_model->deleteSermonFile($sermon_id);
        $result3     = $this->Sermon_model->deleteSermonTag($sermon_id);    
        if (count($result) > 0) {                    
            echo "Deleted Successfully";
        }
    }
   

}
