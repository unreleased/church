<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminBlogController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Blog_model');
        $this->load->model('Important_model');
        $loggedUserId = $this->session->userdata('id');
        if (!isset($loggedUserId)) {
            redirect('admin/login');
        }
    }



    //**********************
    // view Blog
    // *********************
    public function viewBlog()
    { 
        $permission = $this->permission->hasAccess(array('view_blog'));
        if($permission['view_blog']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        }   
        
        if ($this->input->server('REQUEST_METHOD') != 'POST') {

            $data['page_title']         = 'Blog';
            $data['active_link']        = 'blog';
            $data['headerlink']         = $this->load->view('admin_views/templates/headerlink', $data, true);
            $data['header']             = $this->load->view('admin_views/templates/header', '', true);
            $data['sidebar']            = $this->load->view('admin_views/templates/sidebar', '', true);            
               
            $data['content']        = $this->load->view('admin_views/blog/blog', $data, true);
            $dataJS['custom_js']    = array('blog/blog');
            $data['footer']         = $this->load->view('admin_views/templates/footer', $dataJS, true);
            $this->load->view('admin_views/index', $data);
        } else {
            $permission = $this->permission->hasAccess(array('add_blog'));
            if($permission['add_blog']==0){
                $this->session->set_flashdata('error_msg', 'Access Denied');
                redirect('admin/dashboard');
            } 

            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);
            $this->form_validation->set_rules('blog_title', 'Blog Title', 'required');
            if($this->form_validation->run() == FALSE) {
              $this->session->set_flashdata('error_msg', 'Fill all required fields');
              redirect('admin/blog');
            }
            $key = $this->Important_model->generate_key('blog', 'blog_key', 'blog');
            //feature image upload code start
            if($_FILES){ 
                $image_name = $_FILES['blog_feature_image']['name'];
                $config['encrypt_name']  = TRUE;
                $config['upload_path']   = $this->config->item('blog_image_upload_path');
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('blog_feature_image')) {
                    $status = 'error';
                    $data2['msg'] = $this->upload->display_errors('', '');
                    print_r($data['msg']); die;
                } else {
                    $fileName=$this->upload->data();
                    $data['blog_feature_image']=$fileName['file_name'];
                } 
            }
            $data['blog_key']            = $key;
            $data['staff_id']            = $this->session->userdata('id');
            $data['blog_title']          = $clean['blog_title'];
            $data['blog_description']    = $clean['blog_description'];
            $data['blog_created_at']    = date('Y-m-d H:i:s');
            $result = $this->Blog_model->insert('blog', $data);

            $tag = $clean['blog_tag'];
            for($i=0; $i<count($tag); $i++){                    
                $data2['blog_id'] = $result;
                $data2['blog_tag'] = $tag[$i];
                $result2 = $this->Blog_model->insert('blog_tag', $data2);
            }


            if ($result) {
                $this->session->set_flashdata('success_msg', 'Added Successfully');
                redirect('admin/blog');
            }
        }
    }


    


    //************************
    // load Blog table by ajax
    //************************
    public function loadBlogTable()
    {
        $list = $this->Blog_model->get_datatables('blog');
        $data = array();
        $no = $_POST['start'];
        $permission = $this->permission->hasAccess(array('edit_blog','delete_blog'));
        foreach ($list as $blog) {
            $no++;          
            $row = array();

            $blog_id = $blog['blog_id'];
            $total_comment = count($this->Blog_model->getBlogTotalComment($blog_id)); 
            $active_comment = count($this->Blog_model->getBlogActiveComment($blog_id)); 
            $deactive_comment = count($this->Blog_model->getBlogDeactiveComment($blog_id));
            
            $row[]="<input type='checkbox' class='sub_chk' data-id='{$blog["blog_id"]}'>";
            $row[] = $no;            
            $row[] = $blog['blog_title']; 
            $role['comment'] = "<a  href='admin/blog-comment/{$blog["blog_key"]}'>$total_comment Comment</a>";
            $role['comment'] .= "<br><span style='color: green;'>$active_comment Active | </span>";
            $role['comment'] .= "<span style='color: #d73925;'>$deactive_comment Deactive</span>";

            $row[] = $role['comment']; 

            if(!empty($blog['blog_feature_image'])){
                $row[] = "<img src=".$this->config->item('blog_image_source_path').$blog['blog_feature_image']." style='height: 40px; width: 40px'>";
            }
            if(empty($blog['blog_feature_image'])){ $row[] = "";}

            $role['action'] = "";
            if($permission['edit_blog']==1){
            $role['action'].="<a blog-key='{$blog["blog_key"]}' data-toggle='modal' class='edit_blog' data-target='#edit_blog' href='javascript:void(0)'><i style='margin-right:10px; font-size: 16px;'' data-toggle='tooltip' title='Edit' class='fa fa-pencil'></i></a>";
            }
            
            if($permission['edit_blog']==1){
            $role['action'].="<a href='javascript:void(0)'><i data-delete_blog='{$blog["blog_key"]}' style='font-size: 16px' data-toggle='tooltip' title='Delete' class='fa fa-trash btn_delete_blog'></i></a>"; 
            }
            $row[] = $role['action'];
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Blog_model->count_all('blog'),
            "recordsFiltered" => $this->Blog_model->count_filtered('blog'),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
        die();
    }


    //*******************
    // render edit blog
    // ******************
    public function renderEditBlog()
    {
        $blog_key = $this->input->post('blog_key');
        $data['blog_info']   =   $this->Blog_model->getBlogInfo($blog_key);
        $blog_id             = $data['blog_info'][0]['blog_id'];
        $data['all_tag']     = $this->Blog_model->getBlogTag($blog_id); 
        $json = array();
        $json['blog_info']                 = $data['blog_info'];
        $json['all_tag']                   = $data['all_tag'];
        $json['edit_blog_div'] = $this->load->view('admin_views/blog/edit_blog_div', $data, TRUE);
        echo json_encode($json);
    }


     //************************
    // update gallery category
    // ***********************
    public function updateBlog()
    {
        $permission = $this->permission->hasAccess(array('edit_blog'));
        if($permission['edit_blog']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        }    

        $this->form_validation->set_rules('blog_title', 'Blog Title', 'required');        
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_msg', 'Please Fill up the required field');
            redirect('admin/blog');
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post); 

             if(!empty($_FILES['blog_feature_image']['name'])){
                $config['encrypt_name']  = TRUE;
                $config['upload_path']   = $this->config->item('blog_image_upload_path');
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('blog_feature_image')) {
                    $status = 'error';
                    $data2['msg'] = $this->upload->display_errors('', '');
                    print_r($data['msg']); die;
                } else {
                    $fileName=$this->upload->data();
                    $data['blog_feature_image']=$fileName['file_name'];
                } 
             }                     
            $data['blog_title']          = $clean['blog_title'];
            $data['blog_description']    = $clean['blog_description'];
            $data['blog_updated_at']     = date('Y-m-d H:i:s');
            $data['blog_id']             = $clean['blog_id'];

            $result = $this->Blog_model->updateBlog($data);

            $deleteResult = $this->Blog_model->deletePreviousTag($clean['blog_id']); 
            $tag = $clean['blog_tag'];
            for($i=0; $i<count($tag); $i++){                    
                $data2['blog_id'] = $clean['blog_id'];
                $data2['blog_tag'] = $tag[$i];
                $result2 = $this->Blog_model->insert('blog_tag', $data2);
            }
            if ($result) {
                $this->session->set_flashdata('success_msg', 'Updated Successfully');
                redirect('admin/blog');
            }

        }
    }


    //**********************
    // delete Blog
    // *********************
    public function deleteBlog()
    {
        $permission = $this->permission->hasAccess(array('delete_blog'));
        if($permission['delete_blog']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        } 

        $blog_key  = $this->input->post('blog_key');
        $blog_info = $this->Blog_model->getBlogInfo($blog_key);       
        $blog_id   = $blog_info[0]['blog_id'];
        
        $image_name = $blog_info[0]['blog_feature_image'];
        $result = $this->Blog_model->deleteBlog($blog_key);
        if (count($result) > 0) {
             $path = $this->config->item('blog_image_upload_path').'/'.$image_name;
                if(!empty($image_name) && file_exists($path)){
                    unlink($path);
                }
            $remove_tag = $this->Blog_model->deletePreviousTag($blog_id);                  
            echo "Deleted Successfully";
        }
    }

    //**********************
    // Mass blog Delete
    // *********************
    public function massBlogDelete(){
        $permission = $this->permission->hasAccess(array('delete_blog'));
        if($permission['delete_blog']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        } 
        $ids = $this->input->post('ids'); 
        $all_id = explode(",",$ids);
        $result = $this->Blog_model->deleteMassBlog($all_id);
        $result2 = $this->Blog_model->deleteMassTag($all_id);
        if($result){
            $this->session->set_flashdata('success_msg', 'Deleted Successfully');
            echo "Delete Successfully";
        }
    }


    public function blogComment($blog_key){
        if ($this->input->server('REQUEST_METHOD') != 'POST') {

            $data['page_title']         = 'Blog Comment';
            $data['active_link']        = 'blog_comment';
            $data['headerlink']         = $this->load->view('admin_views/templates/headerlink', $data, true);
            $data['header']             = $this->load->view('admin_views/templates/header', '', true);
            $data['sidebar']            = $this->load->view('admin_views/templates/sidebar', '', true);
            $blog_info = $this->Blog_model->getBlogInfo($blog_key);  
            if(empty($blog_info)){
                $this->session->set_flashdata('error_msg', 'Something May Wrong!'); 
                redirect('admin/blog');
            }     
            $blog_id   = $blog_info[0]['blog_id'];
            $data['blog_comment'] = $this->Blog_model->getBlogComment($blog_id);               
            $data['content']        = $this->load->view('admin_views/blog/blog_comment', $data, true);
            $dataJS['custom_js']    = array('blog/blog_comment');
            $data['footer']         = $this->load->view('admin_views/templates/footer', $dataJS, true);
            $this->load->view('admin_views/index', $data);
        } else {
           redirect('admin/blog');
        }
    }


    //*******************************
    // render Edit Blog Comment
    // ******************************
    public function renderEditBlogComment()
    {
        $blog_comment_key = $this->input->post('blog_comment_key');  
        $data['comment_info']   =   $this->Blog_model->get_blog_comment($blog_comment_key);
        $json = array();
        $json['comment_info']     = $data['comment_info'];
        $json['edit_comment_div'] = $this->load->view('admin_views/blog/edit_comment_div', $data, TRUE);
        echo json_encode($json);
    }

    public function updateBlogCommentStatus(){
        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);
        $blog_comment_key = $clean['blog_comment_key'];
        $status = $clean['blog_comment_status'];
        $result = $this->Blog_model->updateCommentStatus($blog_comment_key, $status);

        if($result){
            $comment_info   =   $this->Blog_model->get_blog_comment($blog_comment_key);
            $blog_id = $comment_info[0]['blog_id'];
            $blog_info =  $this->Blog_model->blog_info($blog_id); 
            $blog_key = $blog_info[0]['blog_key'];
            $this->session->set_flashdata('success_msg', 'Update Successfully');
            redirect("admin/blog-comment/".$blog_key);
        }

    }


    //**********************
    // delete Blog
    // *********************
    public function deleteBlogComment()
    {
        $blog_comment_key  = $this->input->post('blog_comment_key');
        $result = $this->Blog_model->deleteBlogComment($blog_comment_key);
        if (count($result) > 0) {                      
            echo "Deleted Successfully";
        }
    }



   

}
